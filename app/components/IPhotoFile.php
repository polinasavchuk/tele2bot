<?php

namespace app\components;

interface IPhotoFile
{
    public function getFileId();
    public function setFileId($file_id);

    public function getFileHandler();
}
