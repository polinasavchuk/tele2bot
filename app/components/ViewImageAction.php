<?php
namespace app\components;

use app\models\File;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class ViewImageAction extends Action
{
    public function run($id)
    {
        $model = File::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        \Yii::$app->response->headers->add('Content-Type', 'image/jpeg');

        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        \Yii::$app->response->data = $model->file;

        return \Yii::$app->response;
    }
}
