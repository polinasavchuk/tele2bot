<?php

namespace app\controllers;

use app\IQBot\Bot\Config;
use app\IQBot\Bot\Bot;

use app\modules\admin\Bot\Telegram\Telegram;
use Longman\TelegramBot\TelegramLog;
use yii\web\Controller;

class HookController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        try {
            TelegramLog::initUpdateLog('php://stderr');
            TelegramLog::initDebugLog('php://stderr');
            TelegramLog::initErrorLog('php://stderr');

            $telegram = new Telegram(Config::BOT_TOKEN, Config::BOT_NAME);
            $telegram->addCommandsPath(\Yii::getAlias('@app/IQBot/Bot/Command'));
            $telegram->setCommandNamespace('app\\IQBot\\Bot\\Command\\');
            $telegram->enableMySql(Config::GetDatabaseCredentials());

            $bot = new Bot($telegram);
            $bot->handle();
        } catch (\Exception $e) {
            /// @todo: rewrite
            Bot::SendMessage(['chat_id' => 92801842, 'text' => $e->getMessage().PHP_EOL.$e->getTraceAsString()]);
        }

        die;
    }
}
