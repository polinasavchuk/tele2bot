<?php

namespace app\models;

use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[MediaMessage]].
 *
 * @see MediaMessage
 */
class MediaMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return MediaMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MediaMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function type($type)
    {
        return $this->andWhere(['type' => $type]);
    }

    public function readyForSpam($timestamp = null)
    {
        $timestamp = $timestamp ?: time();
        return $this->andWhere(new Expression('ROUND(notify_at/60)*60 = ROUND(:timestamp/60)*60', ['timestamp' => $timestamp]));
    }
}
