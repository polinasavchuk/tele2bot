<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hall".
 *
 * @property integer $id
 * @property string $name
 */
class Hall extends \yii\db\ActiveRecord
{
    const FIRST_HALL = 1;
    const SECOND_HALL = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hall';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
