<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Exhibitor]].
 *
 * @see Exhibitor
 */
class ExhibitorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Exhibitor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Exhibitor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
