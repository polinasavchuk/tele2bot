<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\web\UploadedFile;

/**
 * This is the model class for table "speaker".
 *
 * @property integer $id
 * @property integer $id_file
 * @property string $name
 * @property string $company
 * @property string $about
 * @property integer $active
 */
class Speaker extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $imageFile;

    /** @var bool */
    public $deleteImage = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'speaker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_file'], 'integer'],
            [['name'], 'required'],
            [['about'], 'string'],
            [['name', 'company'], 'string', 'max' => 255],
            [['active', 'deleteImage'], 'boolean'],
            [['imageFile'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_file' => 'Id File',
            'name' => 'Имя',
            'company' => 'Должность и компания',
            'about' => 'Описание',
            'active' => 'Активен',
            'imageFile' => 'Фото',
            'deleteImage' => 'Удалить фото',
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_DELETE,
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->deleteImage) {
                $this->id_file = null;
            } else if ($this->imageFile) {
                $file = new File();
                $file->file = file_get_contents($this->imageFile->tempName);
                $file->filename = $this->imageFile->name;

                if (!$file->save()) {
                    throw new Exception("Ошибка сохранения фотографии");
                }

                $this->id_file = $file->id;
            }

            return true;
        }

        return false;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        self::getDb()->createCommand()->delete('{{%speech_speaker}}', ['id_speaker' => $this->id])->execute();
        return true;
    }


    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     * @return SpeakerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SpeakerQuery(get_called_class());
    }
}
