<?php

namespace app\models;

use app\modules\admin\models\User;
use Yii;

/**
 * This is the model class for table "{{%vote}}".
 *
 * @property integer $id_chat
 * @property integer $id_user
 * @property integer $result
 * @property string $note
 *
 * @property User $user
 */
class Vote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vote}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_chat', 'id_user', 'result'], 'required'],
            [['id_chat', 'id_user'], 'integer'],
            [['result'], 'boolean'],
            [['note'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_chat' => 'Чат',
            'id_user' => 'Пользователь',
            'result' => 'Понравилось?',
            'note' => 'Что не понравилось?',
        ];
    }

    /**
     * @inheritdoc
     * @return VoteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VoteQuery(get_called_class());
    }

    public static function WriteYes($chat_id, $user_id)
    {
        self::getDb()->createCommand(
            'INSERT INTO {{%vote}} (id_chat, id_user, result, note) VALUES (:id_chat, :id_user, 1, "") '.
            'ON DUPLICATE KEY UPDATE result=VALUES(result), note=VALUES(note)',
            ['id_chat' => $chat_id, 'id_user' => $user_id])->execute();
    }

    public static function WriteNo($chat_id, $user_id, $note)
    {
        self::getDb()->createCommand(
            'INSERT INTO {{%vote}} (id_chat, id_user, result, note) VALUES (:id_chat, :id_user, 0, :note) '.
            'ON DUPLICATE KEY UPDATE result=VALUES(result), note=VALUES(note)',
            ['id_chat' => $chat_id, 'id_user' => $user_id, 'note' => $note])->execute();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
