<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "media_message".
 *
 * @property integer $id
 * @property string $type
 * @property integer $id_file
 * @property string $message
 * @property integer $notify_at
 * @property integer $pickerTime
 */
class MediaMessage extends \yii\db\ActiveRecord
{
    const TYPE_FLIGHT = 'flight';
    const TYPE_NOTIFY = 'notify';
    const TYPE_PHOTO = 'photo';
    const TYPE_SCHEDULE = 'schedule';

    /** @var UploadedFile */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['id_file', 'notify_at'], 'integer'],
            [['message'], 'default', 'value' => ''],
            [['message', 'pickerTime'], 'string'],
            [['type'], 'string', 'max' => 255],
            [['imageFile'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'id_file' => 'Id File',
            'message' => 'Сообщение',
            'imageFile' => 'Изображение',
            'notify_at' => 'Время рассылки',
            'pickerTime' => 'Время рассылки',
        ];
    }

    /**
     * @inheritdoc
     * @return MediaMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MediaMessageQuery(get_called_class());
    }

    public function getTypeName()
    {
        $types = self::GetTypes();
        return $types[$this->type];
    }

    public function GetTypes()
    {
        return [
            self::TYPE_FLIGHT => 'Рейс',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->imageFile) {
                $file = new File();
                $file->file = file_get_contents($this->imageFile->tempName);
                $file->filename = $this->imageFile->name;

                if (!$file->save()) {
                    throw new \Exception("Ошибка сохранения изображения");
                }

                $this->id_file = $file->id;
            }

            return true;
        }

        return false;
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            return true;
        }

        return false;
    }

    public function getPickerTime()
    {
        return $this->notify_at ? date('Y-m-d H:i', $this->notify_at) : null;
    }

    public function setPickerTime($value)
    {
        if ($value) {
            $this->notify_at = strtotime(preg_replace('/^(.* \d\d:\d\d):\d\d$/', '${1}', $value));
        } else {
            $this->notify_at = null;
        }
    }
}
