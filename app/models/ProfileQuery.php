<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Profile]].
 *
 * @see Profile
 */
class ProfileQuery extends \yii\db\ActiveQuery
{
    public function byTelegramUserId($id)
    {
        return $this->innerJoin('{{%user_profile}}',
            'id = id_profile AND id_user = :id_user',
            ['id_user' => $id]
        );
    }
}
