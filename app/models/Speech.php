<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "speech".
 *
 * @property integer $id
 * @property integer $id_hall
 * @property string $name
 * @property string $about
 * @property string $start
 * @property string $end
 * @property integer $active
 *
 * @property Speaker[] $speakers
 * @property Hall $hall
 */
class Speech extends \yii\db\ActiveRecord
{
    public $speakerIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%speech}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_hall'], 'integer'],
            [['active'], 'boolean'],
            [['id_hall', 'name', 'start', 'end'], 'required'],
            [['about'], 'string'],
            [['start', 'end'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['start', 'validateTimeRange'],
            ['speakerIds', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_hall' => 'Зал',
            'name' => 'Название',
            'about' => 'Описание',
            'start' => 'Начало',
            'end' => 'Конец',
            'active' => 'Активен',
            'speakerIds' => 'Спикеры',
        ];
    }

    /**
     * @inheritdoc
     * @return SpeechQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SpeechQuery(get_called_class());
    }

    public function validateTimeRange($attribute, $params)
    {
        if (strtotime($this->start) >= strtotime($this->end)) {
            $this->addError($attribute, 'Начало доклада должно быть раньше окончания.');
        }
    }

    public function getSpeakers()
    {
        return $this->hasMany(Speaker::class, ['id' => 'id_speaker'])->viaTable('speech_speaker', ['id_speech' => 'id']);
    }

    public function getHall()
    {
        return $this->hasOne(Hall::class, ['id' => 'id_hall']);
    }

    public function loadSpeakerIds()
    {
        if ($this->isNewRecord) {
            throw new \RuntimeException('Cannot load speakers for new Speech model');
        }

        $this->speakerIds = array_keys(
            (new \yii\db\Query())
                ->select('id_speaker')
                ->indexBy('id_speaker')
                ->from('{{%speech_speaker}}')
                ->where(['id_speech' => $this->id])
                ->all()
        );
    }

    public function trimSeconds()
    {
        $pattern = '/^(.* \d\d:\d\d):\d\d$/';

        if ($this->start) {
            $this->start = preg_replace($pattern, '${1}', $this->start);
        }

        if ($this->end) {
            $this->end = preg_replace($pattern, '${1}', $this->end);
        }
    }

    public function formatTime(int $today)
    {
        $todayDate = date("d.m.Y", $today);
        $start = strtotime($this->start);
        $end = strtotime($this->end);

//        $startDate = ($todayDate == date("d.m.Y", $start) ? date("H:i", $start) : date("d.m.Y H:i", $start));
        $startDate = date("H:i", $start);

        return [$startDate, date('H:i', $end)];
    }

    public function formatText(int $currentTimestamp, $full = false)
    {
        list($start, $end) = $this->formatTime($currentTimestamp);
        $text =
            "{$start}-{$end}\n".
            "<b>{$this->name}</b>\n";

        if ($full) {
            if ($this->hall) {
                $text .= "{$this->hall->name}\n";
            }

            if ($this->speakers) {
                $text .= count($this->speakers) > 1 ? "\nСпикеры:\n" : "\nСпикер:\n";
                foreach ($this->speakers as $speaker) {
                    $text .= "- {$speaker->name} /speaker{$speaker->id}\n";
                }
            }

            if ($this->about) {
                $text .= "\n{$this->about}";
            }
        } else {
            if ($this->speakers) {
                $text .= count($this->speakers) > 1 ? "Спикеры: " : "Спикер: ";
                $text .= implode(', ', array_column($this->speakers, 'name'));
                $text .= "\nПодробнее: /speech{$this->id}";
            } else if ($this->about) {
                $text .= "\nПодробнее: /speech{$this->id}";
            }
        }

        return trim($text);
    }
}
