<?php

namespace app\models;

use Longman\TelegramBot\Entities\User;
use Yii;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property integer $id
 * @property integer $number
 * @property string $lastname
 * @property string $firstname
 * @property string $patronymic
 * @property string $company
 * @property string $position
 * @property string $email
 * @property string $phone
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['lastname', 'firstname', 'patronymic', 'company', 'position', 'email'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 2055],
            [['number'], 'number', 'integerOnly' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'patronymic' => 'Отчество',
            'company' => 'Компания',
            'position' => 'Должность',
            'email' => 'Email',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @inheritdoc
     * @return ProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProfileQuery(get_called_class());
    }

    /**
     * @param $email
     * @param $user_id
     * @return null|Profile
     */
    public static function RegisterByEmail($email, $user_id)
    {
        /** @var Profile $model */
        $model = self::find()->andWhere(['email' => $email])->one();
        if (!$model) {
            return null;
        }

        \Yii::$app->db->createCommand()->delete(
            '{{%user_profile}}',
            'id_user = :id_user OR id_profile = :id_profile',
            ['id_user' => $user_id, 'id_profile' => $model->id]
        )->execute();
        \Yii::$app->db->createCommand()->insert('{{%user_profile}}', ['id_user' => $user_id, 'id_profile' => $model->id])->execute();

        return $model;
    }

    public static function RegisterByPhone($phone, User $user)
    {
        $trn = self::getDb()->beginTransaction();

        try {
            $model = self::find()->andWhere(['phone' => $phone])->one();
            if (!$model) {
                $tableName = self::tableName();
                $number = \Yii::$app->db->createCommand("SELECT IFNULL(MAX(number), 0)+1 FROM {$tableName} FOR UPDATE")->queryScalar();

                $model = new Profile();
                $model->number = $number;
                $model->firstname = $user->getFirstName();
                $model->lastname = $user->getLastName();
                $model->phone = $phone;

                $model->save(false);
            }

            \Yii::$app->db->createCommand()->delete(
                '{{%user_profile}}',
                'id_user = :id_user OR id_profile = :id_profile',
                ['id_user' => $user->getId(), 'id_profile' => $model->id]
            )->execute();

            \Yii::$app->db->createCommand()->insert('{{%user_profile}}', ['id_user' => $user->getId(), 'id_profile' => $model->id])->execute();

            $trn->commit();

            return $model;
        } catch (\Exception $e) {
            $trn->rollBack();
        }

        return null;
    }

    public static function RegisterByTicket($number, $user_id)
    {
        $model = self::find()->andWhere(['number' => ltrim($number, '0')])->one();
        if (!$model) {
            return null;
        }

        \Yii::$app->db->createCommand()->delete(
            '{{%user_profile}}',
            'id_user = :id_user OR id_profile = :id_profile',
            ['id_user' => $user_id, 'id_profile' => $model->id]
        )->execute();
        \Yii::$app->db->createCommand()->insert('{{%user_profile}}', ['id_user' => $user_id, 'id_profile' => $model->id])->execute();

        return $model;
    }

    /**
     * Участник пропустил регистрацию
     * @var int $user_id
     * @return bool
     */
    public static function RegisterByNull(int $user_id) : bool
    {
        \Yii::$app->db->createCommand()->delete(
            '{{%user_profile}}',
            'id_user = :id_user',
            ['id_user' => $user_id]
        )->execute();
        \Yii::$app->db->createCommand()->insert('{{%user_profile}}', ['id_user' => $user_id])->execute();

        return true;
    }
}
