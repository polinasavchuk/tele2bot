<?php

namespace app\models;

use yii\web\UploadedFile;


/**
 * This is the model class for table "exhibitor".
 *
 * @property integer $id
 * @property integer $id_file
 * @property string $name
 * @property integer $number
 * @property string $about
 * @property integer $active
 */
class Exhibitor extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $imageFile;

    /** @var bool */
    public $deleteImage = false;

    /** @var null|int */
    public $votes = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exhibitor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_file', 'number'], 'integer'],
            [['number'], 'unique'],
            [['active', 'deleteImage'], 'boolean'],
            [['name', 'number'], 'required'],
            [['about'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['imageFile'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_file' => 'Лого',
            'name' => 'Название',
            'number' => 'Номер',
            'about' => 'Описание',
            'active' => 'Активен',
            'imageFile' => 'Лого',
            'deleteImage' => 'Удалить лого',
        ];
    }

    /**
     * @inheritdoc
     * @return ExhibitorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExhibitorQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->deleteImage) {
                $this->id_file = null;
            } else if ($this->imageFile) {
                $file = new File();
                $file->file = file_get_contents($this->imageFile->tempName);
                $file->filename = $this->imageFile->name;

                if (!$file->save()) {
                    throw new \Exception("Ошибка сохранения логотипа");
                }

                $this->id_file = $file->id;
            }

            return true;
        }

        return false;
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            return true;
        }

        return false;
    }

    public static function CanVote($id_user)
    {
        return \Yii::$app->db->createCommand(
            'SELECT COUNT(*) FROM {{%vote}} WHERE id_user = :id_user', ['id_user' => $id_user]
        )->queryScalar() < 2;
    }

    public static function GetTopByQuestion(int $id_question)
    {
        $results = \Yii::$app->db->createCommand(
            'SELECT number, COUNT(*) as votes FROM {{%vote}} WHERE id_question = :id_question GROUP BY number ORDER BY COUNT(*) DESC LIMIT 5',
            ['id_question' => $id_question]
        )->queryAll();

        $models = [];
        foreach ($results as $result) {
            /* sick */
            $model = Exhibitor::find()->andWhere(['number' => $result['number']])->one();
            $model->votes = $result['votes'];
            $models[] = $model;
        }

        return $models;
    }
}
