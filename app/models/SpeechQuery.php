<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Speech]].
 *
 * @see Speech
 */
class SpeechQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Speech[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Speech|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active() : SpeechQuery
    {
        return $this->andWhere(['active' => true]);
    }

    public function firstHall() : SpeechQuery
    {
        return $this->andWhere(['id_hall' => Hall::FIRST_HALL]);
    }

    public function secondHall() : SpeechQuery
    {
        return $this->andWhere(['id_hall' => Hall::SECOND_HALL]);
    }

    public function current(int $currentTimestamp) : SpeechQuery
    {
        return $this->andWhere(
            'start <= :now AND end > :now',
            [':now' => date('Y-m-d H:i:s', $currentTimestamp)]
        )->orderBy('start');
    }

    public function next(int $currentTimestamp) : SpeechQuery
    {
        return $this->andWhere(
            'start >= :now',
            [':now' => date('Y-m-d H:i:s', $currentTimestamp)]
        )->orderBy('start');
    }
}
