<?php

namespace app\models;

use app\modules\admin\components\Exception;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "sponsor".
 *
 * @property integer $id
 * @property integer $id_file
 * @property string $name
 * @property string $about
 * @property string $type
 * @property string $typeName
 * @property integer $active
 */
class Sponsor extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $imageFile;

    /** @var bool */
    public $deleteImage = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sponsor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_file'], 'integer'],
            [['active', 'deleteImage'], 'boolean'],
            [['name', 'type'], 'required'],
            [['about', 'type'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['imageFile'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_file' => 'Логотип',
            'name' => 'Название',
            'about' => 'Описание',
            'type' => 'Тип',
            'typeName' => 'Тип',
            'active' => 'Активен',
            'imageFile' => 'Лого',
            'deleteImage' => 'Удалить лого',
        ];
    }

    /**
     * @inheritdoc
     * @return SponsorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->deleteImage) {
                $this->id_file = null;
            } else if ($this->imageFile) {
                $file = new File();
                $file->file = file_get_contents($this->imageFile->tempName);
                $file->filename = $this->imageFile->name;

                if (!$file->save()) {
                    throw new Exception("Ошибка сохранения логотипа");
                }

                $this->id_file = $file->id;
            }

            return true;
        }

        return false;
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            return true;
        }

        return false;
    }

    public function getTypeName()
    {
        $types = self::GetTypeNames();
        return $types[$this->type] ?? null;
    }

    public static function GetTypeNames()
    {
        return [
            'gold' => 'Золотой',
            'silver' => 'Серебряный',
            'bronze' => 'Бронзовый',
            'other' => 'Другой',
            'partner' => 'Партнер',
        ];
    }
}
