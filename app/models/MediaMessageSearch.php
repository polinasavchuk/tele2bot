<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class MediaMessageSearch extends MediaMessage
{
    public function rules()
    {
        return [];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search(array $params = [])
    {
        $query = MediaMessage::find();

        $query->type(MediaMessage::TYPE_NOTIFY);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'notify_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}