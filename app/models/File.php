<?php

namespace app\models;

use app\components\IPhotoFile;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Entities\PhotoSize;
use Longman\TelegramBot\Request;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property resource $file
 * @property string $filename
 * @property string $file_id
 */
class File extends \yii\db\ActiveRecord implements IPhotoFile
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file', 'filename'], 'required'],
            [['file'], 'string'],
            [['filename', 'file_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'File',
            'filename' => 'Filename',
            'file_id' => 'File ID',
        ];
    }

    public function getFileId()
    {
        return $this->file_id;
    }

    public function setFileId($file_id)
    {
        $this->file_id = $file_id;
        $this->updateAttributes(['file_id']);
    }

    public function getFileHandler()
    {
        if (!$this->file) {
            $this->file = static::find()->select('file')->where($this->getPrimaryKey(true))->scalar();
        }

        $path = \Yii::getAlias('@runtime/tmp');
        FileHelper::createDirectory($path);

        $fp = fopen($path.DIRECTORY_SEPARATOR.$this->filename, 'w+');
        fwrite($fp, $this->file);
        fflush($fp);
        rewind($fp);

        return $fp;
    }

    public static function SendById($id, $data)
    {
        /** @var File $model */
        $model = static::find()->select(['id', 'file_id', 'filename'])->andWhere(['id' => $id])->one();
        if (!$model) {
            throw new \RuntimeException("File with id '{$id}' not found");
        }


        if ($fileId = $model->getFileId()) {
            $data['photo'] = $fileId;
        } else {
            $data['photo'] = $model->getFileHandler();
        }

        $response = Request::sendPhoto($data);
        if ($response && $response->isOk()) {
            /** @var Message $message */
            $message = $response->getResult();
            /** @var PhotoSize */
            $photo = $message->getPhoto();
            if ($photo) {
                $model->setFileId($photo[0]->getFileId());
            }
        }

        return $response;
    }
}
