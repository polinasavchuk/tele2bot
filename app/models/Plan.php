<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "plan".
 *
 * @property integer $id_file
 */
class Plan extends \yii\db\ActiveRecord
{
    /** @var UploadedFile */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%plan}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imageFile'], 'required'],
            [['id_file'], 'integer'],
            [['imageFile'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_file' => 'Изображение',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->imageFile) {
                $file = new File();
                $file->file = file_get_contents($this->imageFile->tempName);
                $file->filename = $this->imageFile->name;

                if (!$file->save()) {
                    throw new \Exception("Ошибка сохранения логотипа");
                }

                $this->id_file = $file->id;
            }

            return true;
        }

        return false;
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            return true;
        }

        return false;
    }
}
