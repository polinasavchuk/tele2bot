<?php

namespace app\IQBot\Bot;

class Config
{
    const BOT_TOKEN = '483317791:AAEdHtAmcmR6L6v55qHMVgGhFOQLFRZ5jSE';
    const BOT_NAME = 'tele2summit_bot';

    public static function GetDatabaseCredentials()
    {
        $url = parse_url(getenv("DB_DSN"));

        return [
            'host' => $url['host'],
            'database' => substr($url['path'], 1),
            'user' => $url["user"],
            'password' => $url["pass"],

        ];
    }
}
