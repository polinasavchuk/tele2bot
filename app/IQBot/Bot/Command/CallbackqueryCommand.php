<?php

namespace app\IQBot\Bot\Command;

use app\IQBot\Bot\Bot;
use app\IQBot\Bot\Exceptions\PlanMissingException;
use app\models\Exhibitor;
use app\models\Profile;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

/**
 * Class CallbackqueryCommand
 *
 * @property \app\modules\admin\Bot\Telegram\Telegram $telegram
 */
class CallbackqueryCommand extends UserCommand
{
    public function execute()
    {
        $update = $this->getUpdate();
        if (!($callbackQuery = $update->getCallbackQuery())) {
            return new EmptyServerResponse();
        }

        $message = $callbackQuery->getMessage();
        $callbackQueryId = $callbackQuery->getId();
        $callbackData = $callbackQuery->getData();

        Request::answerCallbackQuery([
            'callback_query_id' => $callbackQueryId,
        ]);

        $data = [
            'chat_id' => $message->getChat()->getId(),
            'message_id' => $message->getMessageId(),
        ];

        if ($callbackData == 'speakers-1') {
            return $this->telegram->bot->sendSpeakers1($data);
        }

        if ($callbackData == 'speakers-2') {
            return $this->telegram->bot->sendSpeakers($data);
        }

        if (in_array($callbackData, ['cancel', 'auth-skip', 'cancel-new'])) {
            State::Set($callbackQuery->getFrom()->getId(), $message->getChat()->getId(), State::STATE_DEFAULT);
            return $this->telegram->bot->sendMainScreen($data, $callbackData != 'cancel-new');
        }

        if ($callbackData == 'exponents') {
            return $this->telegram->bot->sendExhibitors($message->getChat()->getId());
        }

        if ($callbackData == 'schedule') {
            return $this->telegram->bot->sendSchedule($data);
        }

        if ($callbackData == 'flights') {
            return $this->telegram->bot->sendFlights($data);
        }

        if ($callbackData == 'photo') {
            return $this->telegram->bot->sendPhotos($data);
        }

        if ($callbackData == 'vote') {
            if (Exhibitor::CanVote($callbackQuery->getFrom()->getId())) {
                $this->telegram->bot->setState(State::STATE_VOTE, $this);
                return $this->telegram->bot->sendVote($data);
            } else {
                return Request::sendMessage([
                    'chat_id' => $message->getChat()->getId(),
                    'text' => 'Вы уже голосовали!',
                    'reply_markup' => Bot::GetMenuReplyMarkup(),
                ]);
            }
        }

        if ($callbackData == 'auth-cancel') {
            State::Set($callbackQuery->getFrom()->getId(), $message->getChat()->getId(), State::STATE_AUTH);
        }

        if ($callbackData == 'auth-skip') {
            Profile::RegisterByNull($callbackQuery->getFrom()->getId());
            State::Set($callbackQuery->getFrom()->getId(), $message->getChat()->getId(), State::STATE_DEFAULT);
            return $this->telegram->bot->sendMainScreen([
                'chat_id' => $message->getChat()->getId(),
                'message_id' => $message->getMessageId(),
            ]);
        }

        if ($callbackData == 'ask-speaker') {
            State::Set($callbackQuery->getFrom()->getId(), $message->getChat()->getId(), State::STATE_ASK_SPEAKER);
        }

        return $this->telegram->bot->handleCommand($this);
    }
}
