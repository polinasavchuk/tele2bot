<?php

namespace app\IQBot\Bot\Command;

use Longman\TelegramBot\Commands\UserCommand;

class SponsorsCommand extends UserCommand
{
    protected $name = 'plan';
    protected $description = 'План выставки';
    protected $usage = '/plan';
    protected $version = '1';

    public function execute()
    {
        $message = $this->getMessage();

        return $this->telegram->bot->sendSponsors($message->getChat()->getId());
    }
}
