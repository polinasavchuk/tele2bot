<?php

namespace app\IQBot\Bot\Command;

use app\models\Question;
use app\models\Stub;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Document;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Entities\ReplyKeyboardMarkup;
use Longman\TelegramBot\Request;

class GenericmessageCommand extends UserCommand
{
    protected $name = 'Genericmessage';
    protected $description = 'Handle generic message';
    protected $version = '1.0.2';
    protected $need_mysql = true;

    public function execute()
    {
        return $this->telegram->bot->handleCommand($this);
    }
}
