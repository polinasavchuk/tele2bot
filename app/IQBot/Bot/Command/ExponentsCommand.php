<?php

namespace app\IQBot\Bot\Command;

use Longman\TelegramBot\Commands\UserCommand;

class ExponentsCommand extends UserCommand
{
    protected $name = 'exponents';
    protected $description = 'Список экспонентов';
    protected $usage = '/exponents';
    protected $version = '1';

    public function execute()
    {
        $message = $this->getMessage();

        return $this->telegram->bot->sendExhibitors($message->getChat()->getId());
    }
}
