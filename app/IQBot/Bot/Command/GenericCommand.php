<?php

namespace app\IQBot\Bot\Command;

use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use Longman\TelegramBot\Commands\UserCommand;

class GenericCommand extends UserCommand
{
    protected $name = 'Generic';
    protected $description = '';
    protected $version = '1.0.1';

    public function execute()
    {
        $message = $this->getMessage();
        if (!$message || !$message->getChat() || !$message->getCommand()) {
            return new EmptyServerResponse();
        }

        $chat_id = $message->getChat()->getId();
        $command = $message->getCommand();

        if (preg_match('/speech(\d+)/ui', $command, $m)) {
            return $this->telegram->bot->sendSpeech($chat_id, $m[1]) ;
        }

        if (preg_match('/speaker(\d+)/ui', $command, $m)) {
            return $this->telegram->bot->sendSpeaker($chat_id, $m[1]) ;
        }

        if (preg_match('/sponsor(\d+)/ui', $command, $m)) {
            return $this->telegram->bot->sendSponsor($chat_id, $m[1]) ;
        }

        if (preg_match('/expo(\d+)/ui', $command, $m)) {
            return $this->telegram->bot->sendExhibitor($chat_id, $m[1]) ;
        }

        if (preg_match('/speakers(1|2)/ui', $command, $m)) {
            if ($m[1] == 1) {
                return $this->telegram->bot->sendSpeakers1($chat_id);
            } else {
                return $this->telegram->bot->sendSpeakers2($chat_id);
            }
        }

        return new EmptyServerResponse();
    }
}
