<?php

namespace app\IQBot\Bot\Command;

use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\UserCommand;

class AuthCommand extends UserCommand
{
    protected $name = 'auth';
    protected $description = 'Авторизация';
    protected $usage = '/auth';
    protected $version = '1.0.1';

    public function execute()
    {
        $message = $this->getMessage();
        State::Set($message->getFrom()->getId(), $message->getChat()->getId(), State::STATE_AUTH);

        return $this->telegram->bot->handleCommand($this);
    }
}
