<?php

namespace app\IQBot\Bot\Command;

use app\IQBot\Bot\Bot;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\SpamForm;
use Longman\TelegramBot\Commands\UserCommand;

class Schedule5Command extends UserCommand
{
    protected $name = 'schedule5';

    public function execute()
    {
        $message = $this->getMessage();
        if (in_array($message->getFrom()->getId(), [92801842])) {
            $form = new SpamForm();
            $form->message = Bot::FormatSchedule5(time());
            $form->send([
                'parse_mode' => 'HTML',
            ]);
        }

        return new EmptyServerResponse();
    }
}
