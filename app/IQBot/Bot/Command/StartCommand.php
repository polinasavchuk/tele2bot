<?php

namespace app\IQBot\Bot\Command;

use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\UserCommand;

class StartCommand extends UserCommand
{
    protected $name = 'start';
    protected $description = 'Пуск';
    protected $usage = '/start';
    protected $version = '1.0.1';

    public function execute()
    {
        $message = $this->getMessage();
        State::Set($message->getFrom()->getId(), $message->getChat()->getId(), 'start');

        return $this->telegram->bot->handleCommand($this);
    }
}
