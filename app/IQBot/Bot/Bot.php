<?php

namespace app\IQBot\Bot;

use app\models\Exhibitor;
use app\models\File;
use app\models\Hall;
use app\models\MediaMessage;
use app\models\Plan;
use app\models\Profile;
use app\models\Speaker;
use app\models\Speech;
use app\models\Sponsor;
use app\models\Vote;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Request;

class Bot extends \app\modules\admin\Bot\Bot
{
    const MENU_MAIN = 'main';
    const MENU_SCHEDULE = 'schedule';
    const MENU_VOTE = 'vote';
    const MENU_AUTH = 'auth';
    const MENU_AUTH_EMAIL = 'auth_email';
    const MENU_CANCEL = 'cancel';
    const MENU_CANCEL_NEW = 'cancel_new';

    public function checkAuthorization($user_id)
    {
        return !!\Yii::$app->db->createCommand('SELECT 1 FROM {{%user_profile}} WHERE id_user = :id_user', ['id_user' =>$user_id])->queryScalar();
    }

    public function sendAuthorization($chat_id)
    {
        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' =>
                "Привет! Я Tele2Bot.\nЯ помогу тебе разобраться в будущем и отвечу на вопросы про конференцию Future is now.\nДля начала, пройди идентификацию.",
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_AUTH),
        ]);
    }

    public function sendAuthorizationMismatch($chat_id)
    {
        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => "Что-то не то 🤔 Пришлите мне *свой* контакт нажав на кнопку внизу 👇",
            'parse_mode' => 'Markdown',
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_AUTH),
        ]);
    }

    public function sendAuthorizationMissingPhone($chat_id)
    {
        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => "К сожалению у вас не указан номер телефона",
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_AUTH),
        ]);
    }

    public function sendAuthorizationSuccess(array $data, $name = null)
    {
        if ($name) {
            $text = "Рад знакомству, {$name}. С чего начнем?";
        } else {
            $text = "Идентификация пройдена успешно. Чем я могу помочь?";
        }

        return Request::sendMessage([
            'text' => $text,
            'reply_markup' => Keyboard::remove(),
        ] + $data);
    }

    public function sendSpeakers($data)
    {
        $speakers = Speaker::find()->andWhere(['active' => true])->orderBy('name')->all();

        if ($speakers) {

            self::SendMessage([
                'text' => "Наши спикеры:",
            ]+$data);

            foreach ($speakers as $speaker) {
                if ($speaker->id_file) {
                    File::SendById($speaker->id_file, [
                        'caption' => "{$speaker->name}\nПодробнее /speaker{$speaker->id}"
                    ]+$data);
                } else {
                    self::SendMessage([
                        'text' => "<b>{$speaker->name}</b>\n{$speaker->company}\nПодробнее /speaker{$speaker->id}",
                        'parse_mode' => 'HTML',
                    ] + $data);
                }
            }
        } else {
            self::SendOrEditMessage([
                'text' => "Нет ни одного спикера 🙊",
                'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_SCHEDULE),
            ] + $data);
        }

        self::SendMessage([
            'text' => "Планнер конференции",
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_SCHEDULE),
        ] + $data);
    }

    public function sendSpeaker($chat_id, $speaker_id)
    {
        $model = Speaker::find()->andWhere(['id' => $speaker_id, 'active' => true])->one();
        if (!$model) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => 'Такого спикера не существует :(',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ]);
        }

        if ($model->id_file) {
            File::SendById($model->id_file, [
                'chat_id' => $chat_id,
            ]);
        }

        $text = "<b>{$model->name}</b>\n";
        if ($model->company) {
            $text .= "{$model->company}\n";
        }

        if ($model->about) {
            $text .= "\n{$model->about}\n";
        }

        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ]);
    }

    public function sendSponsors($chat_id)
    {
        $typeNames = Sponsor::GetTypeNames();
        $texts = array_fill_keys(array_keys($typeNames), []);

        /** @var Sponsor[] $models */
        $models = Sponsor::find()->andWhere(['active' => true])->orderBy('type')->all();
        foreach ($models as $model) {
            $texts[$model->type][] = "{$model->name} /sponsor{$model->id}";
        }

        $text = "Наши спонсоры:";
        foreach ($texts as $type => $groupTexts) {
            if ($groupTexts) {
                $text .= "\n\n<b>".$typeNames[$type] . "</b>\n" . join("\n", $groupTexts);
            }
        }

        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'disable_web_page_preview' => true,
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ]);
    }

    public function sendSponsor($chat_id, $sponsor_id)
    {
        $model = Sponsor::findOne($sponsor_id);
        if (!$model || !$model->active) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => 'Кажется тут ничего нет :\\',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ]);
        }

        if ($model->id_file) {
            File::SendById($model->id_file, ['chat_id' => $chat_id]);
        }

        $text = "<b>{$model->name}</b>";
        if ($model->about) {
            $text .= "\n".$model->about;
        }

        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'disable_web_page_preview' => true,
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ]);
    }

    public function sendExhibitor($chat_id, $number)
    {
        $model = Exhibitor::find()->andWhere(['number' => $number])->one();
        if (!$model || !$model->active) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => 'Кажется тут ничего нет :\\',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ]);
        }

        if ($model->id_file) {
            File::SendById($model->id_file, ['chat_id' => $chat_id]);
        }

        $text = "<b>{$model->name}</b>\nСтенд #{$model->number}";
        if ($model->about) {
            $text .= "\n".$model->about;
        }

        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'disable_web_page_preview' => true,
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ]);
    }

    /**
     * Разсписание отправляемое за 5 минут до начала доклада
     * @param $chat_id
     * @return EmptyServerResponse|\Longman\TelegramBot\Entities\ServerResponse
     */
    public function sendSchedule5($chat_id)
    {
        if ($text = self::FormatSchedule5(time())) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => $text,
                'parse_mode' => 'HTML',
                'disable_web_page_preview' => true,
                'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_SCHEDULE),
            ]);
        }

        return new EmptyServerResponse();
    }

    public function sendSchedule($data)
    {
        return self::SendOrEditMessage([
            'text' => 'Планнер конференции',
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_SCHEDULE),
        ] + $data);
    }

    public function sendSpeakers1($data)
    {
        $media = MediaMessage::find()->type(MediaMessage::TYPE_SCHEDULE)->one();

        if ($media) {
            if ($media->id_file) {
                $fileData = [
                    'caption' => 'Расписание',
                ] + $data;


                if (!$media->message) {
                    $fileData['reply_markup'] = Bot::GetMenuReplyMarkup();
                }

                File::SendById($media->id_file, $fileData);
            }

            if ($media->message) {
                $data = [
                    'text' => $media->message,
                    'reply_markup' => Bot::GetMenuReplyMarkup(),
                ] + $data;

                return Request::sendMessage($data);
            }
        } else {
            return self::SendOrEditMessage([
                'text' => "Расписание не задано ⛔️",
                'parse_mode' => 'HTML',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ] + $data);
        }
    }

    public function sendSpeakers2($chat_id)
    {
        return $this->sendHallSpeakers($chat_id, Hall::SECOND_HALL);
    }

    public function sendMainScreen(array $data, $tryEdit = true)
    {
        $data = array_merge($data, [
            'text' => "Главное меню",
            'parse_mode' => 'HTML',
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ]);

        return $tryEdit ? self::SendOrEditMessage($data) : self::SendMessage($data);
    }

    public function sendSpeech($chat_id, $speech_id)
    {
        $model = Speech::find()->andWhere(['id' => $speech_id])->active()->one();
        if (!$model) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => 'Такого доклада не существует :(',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ]);
        }

        $text = $model->formatText(time(), true);

        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ]);
    }

    public static function FormatSchedule5($now)
    {
        $text = self::FormatSchedule("1 этаж:\n", array_filter([
            Speech::find()->with('speakers')->active()->firstHall()->next($now)->limit(1)->one()
        ]));

        $text .= self::FormatSchedule("\n2 этаж:", array_filter([
            Speech::find()->with('speakers')->active()->secondHall()->next($now)->limit(1)->one()
        ]));

        if ($text) {
            $text = "Через несколько минут начнутся следующие доклады:\n".$text;
        }

        return $text;
    }

    /**
     * @param string $section
     * @param Speech[] $models
     * @return string
     */
    protected static function FormatSchedule(string $section, $models) : string
    {
        if (!$models) {
            return '';
        }

        $text = $section."\n";
        foreach ($models as $model) {
            $text .= $model->formatText(time())."\n";
        }

        return $text;
    }

    protected function sendHallSpeakers($data, $hall_id)
    {
        /** @var Speech[] $models */
        $models = Speech::find()
            ->with('speakers')
            ->andWhere(['id_hall' => $hall_id, 'active' => true])
            ->orderBy('start')
            ->all();

        if (!$models) {
            return Request::sendMessage([
                'text' => 'Кажется тут ничего нет :\\',
                'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_SCHEDULE),
            ] + $data);
        }

        $now = time();
        $text = "";
        foreach ($models as $model) {
            $text .= $model->formatText($now)."\n\n";
        }

        self::SendMessage([
            'text' => $text,
            'parse_mode' => 'HTML',
            'disable_web_page_preview' => true,
        ] + $data);

        self::SendMessage([
                'text' => "Планнер конференции",
                'parse_mode' => 'HTML',
                'disable_web_page_preview' => true,
                'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_SCHEDULE),
            ] + $data);
    }

    public function sendAskSpeaker(array $data)
    {
        return self::SendOrEditMessage([
            'text' => "Задай свой вопрос и укажи имя спикера.",
            'parse_mode' => 'HTML',
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_CANCEL),
        ] + $data);
    }

    public function sendAskSpeakerConfirmation(array $data, $text, \Longman\TelegramBot\Entities\User $from)
    {
        $text = "Вопрос от пользователя:\n";
        $text .= implode(' ', array_filter([$from->getFirstName(), $from->getLastName(), $from->getUsername()]));
        $text .= "\n\n{$text}";

        self::SendMessage([
            'chat_id' => '@Tele2_SUMMIT',
            'text' => $text,
        ]);

        return self::SendOrEditMessage([
            'text' => "Вопрос принят. Скоро ты получишь ответ.",
            'parse_mode' => 'HTML',
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ] + $data);
    }

    public function sendVote(array $data)
    {
        return self::SendOrEditMessage([
            'text' => "Всё ли понравилось на конференции?",
            'parse_mode' => 'HTML',
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_VOTE),
        ] + $data);
    }

    public function sendVoteQuestion(array $data)
    {
        return self::SendOrEditMessage([
            'text' => "А что именно не понравилось?",
            'parse_mode' => 'HTML',
            'reply_markup' => Bot::GetMenuReplyMarkup(self::MENU_CANCEL),
        ] + $data);
    }

    public function sendVoteAnswered(array $data)
    {
        return self::SendOrEditMessage([
                'text' => "Спасибо за честность. Будем работать!",
                'parse_mode' => 'HTML',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ] + $data);
    }

    public function sendVoteYes(array $data)
    {
        return self::SendOrEditMessage([
            'text' => 'Спасибо, мы очень рады 😊',
            'reply_markup' => Bot::GetMenuReplyMarkup(),
        ] + $data);
    }

    public function sendFlights($data)
    {
        $media = MediaMessage::find()->type(MediaMessage::TYPE_FLIGHT)->one();

        if ($media) {
            if ($media->id_file) {
                $fileData = [
                    'caption' => 'Расписание рейсов',
                ] + $data;


                if (!$media->message) {
                    $fileData['reply_markup'] = Bot::GetMenuReplyMarkup();
                }

                File::SendById($media->id_file, $fileData);
            }

            if ($media->message) {
                $data = [
                    'text' => $media->message,
                    'reply_markup' => Bot::GetMenuReplyMarkup(),
                ] + $data;

                return Request::sendMessage($data);
            }
        } else {
            return self::SendOrEditMessage([
                'text' => "Расписание рейсов не задано ⛔️",
                'parse_mode' => 'HTML',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ] + $data);
        }
    }

    public function sendPhotos(array $data)
    {
        $media = MediaMessage::find()->type(MediaMessage::TYPE_PHOTO)->one();

        if ($media) {
            if ($media->id_file) {
                $fileData = $data;
                if (!$media->message) {
                    $fileData['reply_markup'] = Bot::GetMenuReplyMarkup();
                }

                File::SendById($media->id_file, $fileData);
            }

            if ($media->message) {
                $data = [
                    'text' => $media->message,
                    'reply_markup' => Bot::GetMenuReplyMarkup(),
                    'disable_web_page_preview' => true,
                ] + $data;

                return Request::sendMessage($data);
            }
        } else {
            return Request::sendMessage([
                'text' => 'Фото пока нет',
                'reply_markup' => Bot::GetMenuReplyMarkup(),
            ] + $data);
        }
    }

    public function sendWelcome(Profile $profile, array $data = [])
    {
        if ($profile->firstname) {
            return Request::sendMessage(array_merge($data, [
                'text' => "Добро пожаловать, {$profile->firstname}.",
            ]));
        }

        return new EmptyServerResponse();
    }

    public static function GetMenuReplyMarkup($type = self::MENU_MAIN)
    {
        switch ($type)
        {
        case self::MENU_SCHEDULE:
        case self::MENU_MAIN:
            $keyboard = new InlineKeyboard(
                [
                    ['text' => 'Покажи мне расписание', 'callback_data' => 'speakers-1'],
                ],
                [
                    ['text' => 'Помоги мне задать вопрос спикеру', 'callback_data' => 'ask-speaker'],
                ],
                [
                    ['text' => '📊 Опрос', 'callback_data' => 'vote'],
                    ['text' => 'Хочу посмотреть фотографии с конференции', 'callback_data' => 'photo']
                ]
            );
            break;
        case self::MENU_SCHEDULE:
            $keyboard = new InlineKeyboard(
                [
                    ['text' => 'Покажи мне расписание', 'callback_data' => 'speakers-1'],
                ],
                [
                    ['text' => 'Скажи, кто будет выступать на конференции?', 'callback_data' => 'speakers-2'],
                ],
                [
                    ['text' => '↩️ Назад', 'callback_data' => 'cancel'],
                ]
            );
            break;
        case self::MENU_VOTE:
            $keyboard = new InlineKeyboard(
                [
                    ['text' => '✅ Да', 'callback_data' => 'vote-yes'],
                    ['text' => '🚫 Нет', 'callback_data' => 'vote-no'],
                ],
                [
                    ['text' => '⁉️ Если нет, то что', 'callback_data' => 'vote-no-why'],
                ],
                [
                    ['text' => '↩️ Назад', 'callback_data' => 'cancel'],
                ]
            );
            break;
        case self::MENU_AUTH:
            $keyboard = new Keyboard(
                [
                    ['text' => 'Отправить номер телефона', 'request_contact' => true],
                ]
            );

            $keyboard->setResizeKeyboard(true);
            break;
        case self::MENU_AUTH_EMAIL:
            $keyboard = new InlineKeyboard(
                [
                    ['text' => 'Назад', 'callback_data' => 'auth-cancel'],
                ]
            );
            break;
        case self::MENU_CANCEL:
            $keyboard = new InlineKeyboard(
                [
                    ['text' => '↩️ Назад', 'callback_data' => 'cancel'],
                ]
            );
            break;
        case self::MENU_CANCEL_NEW:
            $keyboard = new InlineKeyboard(
                [
                    ['text' => '↩️ Назад', 'callback_data' => 'cancel-new'],
                ]
            );
            break;
        default:
            throw new \RuntimeException("Unknown menu type: {$type}");
        }

        return $keyboard;
    }
}
