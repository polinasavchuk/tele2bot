<?php

namespace app\IQBot\Controllers;

use app\IQBot\Bot\Bot;
use app\models\Exhibitor;
use app\models\Vote;
use app\modules\admin\Bot\Controllers\StateController;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Request;

class Vote2StateController extends StateController
{
    public function handle(Command $command, $forward = false)
    {
        $update = $command->getUpdate();
        switch ($update->getUpdateType()) {
            case 'message':
                return $this->handleMessage($command, $forward);
            case 'callback_query':
                return $this->handleCallbackQuery($command);
            default:
                return false;
        }
    }

    protected function handleMessage(Command $command, $forward)
    {
        /** @var Message $message */
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        if (!$forward) {
            $text = trim($message->getText());

            if ($text) {
                Vote::WriteNo($message->getChat()->getId(), $from->getId(), $text);
                $this->bot->setState(State::STATE_DEFAULT);
                return $this->bot->sendVoteAnswered($data);
            }
        }

        return $this->bot->sendVoteQuestion($data);
    }

    protected function handleCallbackQuery(Command $command)
    {
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        return $this->bot->sendVoteQuestion($data);
    }
}
