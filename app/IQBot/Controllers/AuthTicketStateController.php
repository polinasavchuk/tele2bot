<?php

namespace app\IQBot\Controllers;

use app\IQBot\Bot\Bot;
use app\models\Profile;
use app\modules\admin\Bot\Controllers\StateController;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\ProfileUploadForm;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Request;

class AuthTicketStateController extends StateController
{
    public function handle(Command $command, $forward = false)
    {
        $update = $command->getUpdate();
        switch ($update->getUpdateType()) {
            case 'message':
                $message = $update->getMessage();
                break;
            case 'callback_query':
                $message = $update->getCallbackQuery()->getMessage();
                break;
            default:
                return new EmptyServerResponse();
        }

        if (!$message) {
            return new EmptyServerResponse();
        }

        $data = [
            'chat_id' => $message->getChat()->getId(),
        ];

        if ($forward) {
            $data['text'] = 'Введите номер Вашего билета.';
            $data['reply_markup'] = Bot::GetMenuReplyMarkup(Bot::MENU_AUTH_EMAIL);
        } else {
            $number = trim($message->getText());
            if (!$number) {
                $data['text'] = 'Введите номер Вашего билета.';
                $data['reply_markup'] = Bot::GetMenuReplyMarkup(Bot::MENU_AUTH_EMAIL);
            } else {
                if ($profile = Profile::RegisterByTicket($number, $message->getFrom()->getId())) {
                    $this->bot->sendWelcome($profile, $data);
                    return $this->forward(State::STATE_DEFAULT, $command);
                } else {
                    $data['text'] = 'Участник с таким номером не найден.';
                    $data['reply_markup'] = Bot::GetMenuReplyMarkup(Bot::MENU_AUTH_EMAIL);
                }
            }
        }

        return Request::sendMessage($data);
    }
}
