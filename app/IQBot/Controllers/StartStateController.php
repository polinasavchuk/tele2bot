<?php

namespace app\IQBot\Controllers;

use app\modules\admin\Bot\Controllers\StateController;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Entities\Message;

class StartStateController extends StateController
{
    public function handle(Command $command, $forward = false)
    {
        /** @var Message $message */
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        if (!$this->bot->checkAuthorization($message->getFrom()->getId())) {
            return $this->forward(State::STATE_AUTH, $command);
        }

        return $this->bot->sendMainScreen($data);
    }
}
