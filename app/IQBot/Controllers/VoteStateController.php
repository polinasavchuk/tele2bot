<?php

namespace app\IQBot\Controllers;

use app\IQBot\Bot\Bot;
use app\models\Exhibitor;
use app\models\Vote;
use app\modules\admin\Bot\Controllers\StateController;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Request;

class VoteStateController extends StateController
{
    public function handle(Command $command, $forward = false)
    {
        $update = $command->getUpdate();
        switch ($update->getUpdateType()) {
            case 'message':
                return $this->handleMessage($command);
            case 'callback_query':
                return $this->handleCallbackQuery($command);
            default:
                return false;
        }
    }

    protected function handleMessage(Command $command)
    {
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        return $this->bot->sendVote($data);
    }

    protected function handleCallbackQuery(Command $command)
    {
        /** @var Message $message */
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        $callbackQuery = $command->getUpdate()->getCallbackQuery();
        $callbackData = $callbackQuery->getData();

        if ($callbackData == 'vote-yes') {
            Vote::WriteYes($message->getChat()->getId(), $from->getId());
            $this->bot->setState(State::STATE_DEFAULT);
            return $this->bot->sendVoteYes($data);
        } else if ($callbackData == 'vote-no') {
            Vote::WriteNo($message->getChat()->getId(), $from->getId(), '');
            $this->bot->setState(State::STATE_DEFAULT);
            return $this->bot->sendVoteAnswered($data);
        } else if ($callbackData == 'vote-no-why') {
            return $this->forward(State::STATE_VOTE_2, $command);
        }

        return $this->bot->sendVote($data);
    }
}
