<?php

namespace app\IQBot\Controllers;

use app\models\Profile;
use app\modules\admin\Bot\Controllers\StateController;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\ProfileUploadForm;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Entities\CallbackQuery;
use Longman\TelegramBot\Entities\Message;

class AuthStateController extends StateController
{
    public function handle(Command $command, $forward = false)
    {
        $update = $command->getUpdate();
        switch ($update->getUpdateType()) {
        case 'message':
            return $this->handleMessage($update->getMessage(), $command);
        default:
            return new EmptyServerResponse();
        }
    }

    protected function handleCallbackQuery(CallbackQuery $callbackQuery, Command $command)
    {
        $message = $callbackQuery->getMessage();
        $callbackQueryId = $callbackQuery->getId();
        $callbackData = $callbackQuery->getData();

        if ($callbackData == 'auth-email') {
            return $this->forward(State::STATE_AUTH_EMAIL, $command);
        }

        if ($callbackData == 'auth-phone') {
            return $this->forward(State::STATE_AUTH_PHONE, $command);
        }

        if ($callbackData == 'auth-ticket') {
            return $this->forward(State::STATE_AUTH_TICKET, $command);
        }

        return $this->handleMessage($message, $command);
    }

    protected function handleMessage(Message $message, Command $command)
    {
        /** @var Message $message */
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        $contact = $message->getContact();
        if ($contact) {
            if ($contact->getUserId() !== $message->getFrom()->getId()) {
                return $this->bot->sendAuthorizationMismatch($message->getChat()->getId());
            }

            $phone = $contact->getPhoneNumber();
            $phone = ProfileUploadForm::preparePhone($phone);

            if (!$phone) {
                return $this->bot->sendAuthorizationMissingPhone($message->getChat()->getId());
            }


            if ($profile = Profile::RegisterByPhone($phone, $message->getFrom())) {
                $this->bot->sendAuthorizationSuccess($data, $message->getFrom()->getFirstName());
                return $this->forward(State::STATE_DEFAULT, $command);
            }
        }

        return $this->bot->sendAuthorization($message->getChat()->getId());
    }
}
