<?php

namespace app\IQBot\Controllers;

use app\modules\admin\Bot\Controllers\StateController;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Entities\Message;

class AskSpeakerStateController extends StateController
{
    public function handle(Command $command, $forward = false)
    {
        $update = $command->getUpdate();
        switch ($update->getUpdateType()) {
            case 'message':
                return $this->handleMessage($command, $forward);
            case 'callback_query':
                return $this->handleCallbackQuery($command);
            default:
                return false;
        }
    }

    protected function handleMessage(Command $command, $forward)
    {
        /** @var Message $message */
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        if (!$forward) {
            $text = trim($message->getText());

            if ($text) {
                $this->bot->setState(State::STATE_DEFAULT);
                return $this->bot->sendAskSpeakerConfirmation($data, $text, $from);
            }
        }

        return $this->bot->sendAskSpeaker($data);
    }

    protected function handleCallbackQuery(Command $command)
    {
        $data = $this->getDefaultData($command, $from, $message);
        if (!$data) {
            return false;
        }

        return $this->bot->sendAskSpeaker($data);
    }
}
