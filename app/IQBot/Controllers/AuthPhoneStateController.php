<?php

namespace app\IQBot\Controllers;

use app\IQBot\Bot\Bot;
use app\models\Profile;
use app\modules\admin\Bot\Controllers\StateController;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\ProfileUploadForm;
use app\modules\admin\models\State;
use Longman\TelegramBot\Commands\Command;
use Longman\TelegramBot\Entities\CallbackQuery;
use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Request;

class AuthPhoneStateController extends StateController
{
    public function handle(Command $command, $forward = false)
    {
        $update = $command->getUpdate();
        switch ($update->getUpdateType()) {
            case 'message':
                $message = $update->getMessage();
                break;
            case 'callback_query':
                $message = $update->getCallbackQuery()->getMessage();
                break;
            default:
                return new EmptyServerResponse();
        }

        if (!$message) {
            return new EmptyServerResponse();
        }

        $data = [
            'chat_id' => $message->getChat()->getId(),
        ];

        if ($forward) {
            $data['text'] = 'Введите номер телефона с которым Вы регистрировались. В формате +79261234567';
            $data['reply_markup'] = Bot::GetMenuReplyMarkup(Bot::MENU_AUTH_EMAIL);
        } else {
            $phone = trim($message->getText());
            $phone = ProfileUploadForm::preparePhone($phone);
            if (!$phone) {
                $data['text'] = 'Введите номер телефона в формате +79261234567.';
                $data['reply_markup'] = Bot::GetMenuReplyMarkup(Bot::MENU_AUTH_EMAIL);
            } else {
                if ($profile = Profile::RegisterByPhone($phone, $message->getFrom()->getId())) {
                    $this->bot->sendWelcome($profile, $data);
                    return $this->forward(State::STATE_DEFAULT, $command);
                } else {
                    $data['text'] = 'Участник с таким номером не найден.';
                    $data['reply_markup'] = Bot::GetMenuReplyMarkup(Bot::MENU_AUTH_EMAIL);
                }
            }
        }

        return Request::sendMessage($data);
    }
}
