<?php

use app\models\Profile;

use yii\db\Migration;

class m170305_123831_profile_number extends Migration
{
    public function up()
    {
        Profile::deleteAll();
        self::getDb()->createCommand()->delete('{{%user_profile}}')->execute();
        $this->addColumn(Profile::tableName(), 'number', 'INT UNSIGNED NOT NULL AFTER id');
        $this->createIndex('number', Profile::tableName(), 'number', true);
    }

    public function down()
    {
        $this->dropColumn(Profile::tableName(), 'number');
    }
}
