<?php

use yii\db\Migration;

class m170129_124438_sponsor extends Migration
{
    const TABLE_NAME = 'sponsor';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',
            'id_file' => 'INT UNSIGNED DEFAULT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
            'about' => 'TEXT DEFAULT NULL',
            'type' => 'ENUM("gold", "silver", "bronze", "partner") NOT NULL',
            'active' => 'TINYINT(1) UNSIGNED DEFAULT 1',
            'PRIMARY KEY (id)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
