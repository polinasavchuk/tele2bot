<?php

use yii\db\Migration;

class m170218_070523_other_sponsors extends Migration
{
    public function up()
    {
        $this->alterColumn(\app\models\Sponsor::tableName(), 'type', 'ENUM("gold", "silver", "bronze", "partner", "other") NOT NULL');
    }

    public function down()
    {
        $this->alterColumn(\app\models\Sponsor::tableName(), 'type', 'ENUM("gold", "silver", "bronze", "partner") NOT NULL');
    }
}
