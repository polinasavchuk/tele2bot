<?php

use yii\db\Migration;

class m170205_132626_plan extends Migration
{
    const TABLE_NAME = 'plan';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id_file' => 'INT UNSIGNED NOT NULL',
            'PRIMARY KEY (id_file)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
