<?php

use yii\db\Migration;

class m170211_055638_multi_speaker extends Migration
{
    const TABLE_NAME = '{{%speech_speaker}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id_speech' => 'INT UNSIGNED NOT NULL',
            'id_speaker' => 'INT UNSIGNED NOT NULL',
            'PRIMARY KEY (id_speech, id_speaker)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');

        $this->execute('INSERT INTO '.self::TABLE_NAME.' SELECT id, id_speaker FROM speech');
        $this->dropColumn(\app\models\Speech::tableName(), 'id_speaker');
    }

    public function down()
    {
        $this->addColumn(\app\models\Speech::tableName(), 'id_speaker', 'INT UNSIGNED DEFAULT NULL AFTER id');
        $this->execute('UPDATE speech AS s JOIN '.self::TABLE_NAME.' AS ss ON ss.id_speech = s.id SET s.id_speaker = ss.id_speaker');
        $this->dropTable(self::TABLE_NAME);
    }
}
