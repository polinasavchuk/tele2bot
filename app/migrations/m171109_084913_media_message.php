<?php

use yii\db\Migration;

class m171109_084913_media_message extends Migration
{
    public function up()
    {
        $this->createTable('{{%media_message}}', [
            'id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',
            'type' => 'VARCHAR(255) NOT NULL',
            'id_file' => 'INT UNSIGNED DEFAULT NULL',
            'message' => 'TEXT NOT NULL',
            'notify_at' => 'INT UNSIGNED DEFAULT NULL',
            'PRIMARY KEY(id)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');
    }

    public function down()
    {
        $this->dropTable('{{%media_message}}');
    }
}
