<?php

use yii\db\Migration;

class m170221_213114_vote extends Migration
{
    const TABLE_NAME = '{{%vote}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id_chat' => 'INT UNSIGNED NOT NULL',
            'id_user' => 'INT UNSIGNED NOT NULL',
            'result' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
            'note' => 'TEXT NOT NULL',
            'PRIMARY KEY (id_chat, id_user)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
