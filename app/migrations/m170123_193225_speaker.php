<?php

use yii\db\Migration;

class m170123_193225_speaker extends Migration
{
    const TABLE_NAME = 'speaker';

    const FILE_TABLE_NAME = 'file';

    public function up()
    {
        $this->createTable(self::FILE_TABLE_NAME, [
            'id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',
            'file' => 'LONGBLOB NOT NULL',
            'filename' => 'VARCHAR(255) NOT NULL',
            'file_id' => 'VARCHAR(255) DEFAULT NULL',
            'PRIMARY KEY (id)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');

        $this->createTable(self::TABLE_NAME, [
            'id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',
            'id_file' => 'INT UNSIGNED DEFAULT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
            'company' => 'VARCHAR(255) DEFAULT NULL',
            'about' => 'TEXT DEFAULT NULL',
            'active' => 'TINYINT(1) UNSIGNED DEFAULT 1',
            'PRIMARY KEY (id)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');
    }

    public function down()
    {
        $this->dropTable(self::FILE_TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}

