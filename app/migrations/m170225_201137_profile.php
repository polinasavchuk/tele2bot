<?php

use yii\db\Migration;

class m170225_201137_profile extends Migration
{
    const TABLE_NAME = '{{%profile}}';
    const TABLE_LINK_NAME = '{{%user_profile}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',
            'lastname' => 'VARCHAR(255) DEFAULT NULL',
            'firstname' => 'VARCHAR(255) DEFAULT NULL',
            'patronymic' => 'VARCHAR(255) DEFAULT NULL',
            'company' => 'VARCHAR(255) DEFAULT NULL',
            'position' => 'VARCHAR(255) DEFAULT NULL',
            'email' => 'VARCHAR(255) DEFAULT NULL',
            'phone' => 'VARCHAR(2055) DEFAULT NULL',
            'PRIMARY KEY (id)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');

        $this->createTable(self::TABLE_LINK_NAME, [
            'id_user' => 'INT UNSIGNED NOT NULL',
            'id_profile' => 'INT UNSIGNED DEFAULT NULL',
            'PRIMARY KEY (id_user)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        $this->dropTable(self::TABLE_LINK_NAME);
    }
}
