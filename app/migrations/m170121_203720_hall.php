<?php

use yii\db\Migration;

class m170121_203720_hall extends Migration
{
    const TABLE_NAME = 'hall';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',
            'name' => 'VARCHAR(255) NOT NULL',
            'PRIMARY KEY (id)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci');

        $this->batchInsert(self::TABLE_NAME, ['name'], [['Главный зал']]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
