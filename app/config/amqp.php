<?php

$config = parse_url(getenv('AMQP_DSN'));

return [
    'class' => 'app\modules\admin\components\AmqpComponent',
    'host' => $config['host'],
    'port' => isset($config['port']) ? $config['port'] : 5672,
    'user' => isset($config['user']) ? $config['user'] : '',
    'pass' => isset($config['pass']) ? $config['pass'] : '',
    'path' => isset($config['path']) ? substr($config['path'], 1) ?: '/' : '',
];

