<?php

$url = parse_url(getenv('DB_DSN'));
$db = substr($url['path'], 1);

return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host={$url['host']};dbname={$db}",
    'username' => $url['user'],
    'password' => $url['pass'],
    'charset' => 'utf8mb4',
];

