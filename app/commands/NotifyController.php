<?php

namespace app\commands;

use app\models\MediaMessage;
use app\IQBot\Bot\Config;
use app\IQBot\Bot\Bot;
use app\models\File;
use app\modules\admin\Bot\Telegram\Telegram;
use Longman\TelegramBot\TelegramLog;


class NotifyController extends \yii\console\Controller
{
    protected $telegram;

    public function init()
    {
        parent::init();

        //TelegramLog::initUpdateLog('php://stderr');
        //TelegramLog::initDebugLog('php://stderr');
        TelegramLog::initErrorLog('php://stderr');

        $this->telegram = new Telegram(Config::BOT_TOKEN, Config::BOT_NAME);
        $this->telegram->addCommandsPath(\Yii::getAlias('@app/IQBot/Bot/Command'));
        $this->telegram->setCommandNamespace('app\\IQBot\\Bot\\Command\\');
        $this->telegram->enableMySql(Config::GetDatabaseCredentials());
    }

    public function actionIndex()
    {
        set_time_limit(0);

        /** @var MediaMessage[] $models */
        $models = MediaMessage::find()->type(MediaMessage::TYPE_NOTIFY)->readyForSpam()->all();

        if ($models) {
            $count = count($models);
            $this->stdout("Found {$count} notifications\n");
        }

        foreach ($models as $model) {
            $query = (new \yii\db\Query())
                ->from(\app\modules\admin\models\Chat::tableName())
                ->select(['id', 'type'])
                ->orderBy('id DESC');

            foreach ($query->each() as $chat) {
                try {
                    $this->stdout("Sending notification in chat: {$chat['id']}\n");
                    $data = [
                        'chat_id' => $chat['id'],
                    ];

                    if ($model->id_file) {
                        $fileData = $data;
                        if (!$model->message) {
                            $fileData['reply_markup'] = Bot::GetMenuReplyMarkup(Bot::MENU_CANCEL_NEW);
                        }

                        File::SendById($model->id_file, $fileData);
                    }

                    if ($model->message) {
                        $data = [
                            'text' => $model->message,
                            'reply_markup' => Bot::GetMenuReplyMarkup(Bot::MENU_CANCEL_NEW),
                            'disable_web_page_preview' => true,
                            'parse_mode' => 'Markdown',
                        ] + $data;

                        Bot::sendMessage($data);
                    }

                    usleep(34000);
                } catch (\Exception $e) {
                    $this->stderr($e);
                }
            }
        }
    }
}