<?php

namespace app\commands;

use app\IQBot\Bot\Bot;
use app\IQBot\Bot\Config;
use app\modules\admin\Bot\Telegram\Telegram;
use Longman\TelegramBot\TelegramLog;
use yii\console\Controller;

class HookController extends Controller
{
    public $timeout = 0;

    public function options($actionID)
    {
        return ['timeout'];
    }

    public function actionIndex()
    {
        try {
            $halt = false;
            \pcntl_signal(SIGTERM, function() use (&$halt) {
                $halt = true;
            });

            //TelegramLog::initUpdateLog('php://stderr');
            //TelegramLog::initDebugLog('php://stderr');
            TelegramLog::initErrorLog('php://stderr');

            $telegram = new Telegram(Config::BOT_TOKEN, Config::BOT_NAME);
            $telegram->addCommandsPath(\Yii::getAlias('@app/IQBot/Bot/Command'));
            $telegram->setCommandNamespace('app\\IQBot\\Bot\\Command\\');
            $telegram->enableMySql(Config::GetDatabaseCredentials());

            $telegram->deleteWebhook();

            $bot = new Bot($telegram);

            $startTime = time();

            while(!$halt) {
                try {
                    $bot->handleGetUpdates();
                } catch (\Exception $e) {
                    $this->handleException($e);
                }

                if ($this->timeout >= 0 && $startTime + $this->timeout <= time()) {
                    $this->stdout("Exit by timeout; start: $startTime; timeout: {$this->timeout}\n");
                    $halt = true;
                }
            }
        } catch (\Exception $e) {
            $this->handleException($e);
            exit(1);
        }

        exit;
    }

    protected function handleException(\Exception $e)
    {
        $this->stderr($e->__toString());
        Bot::SendMessage(['chat_id' => 92801842, 'text' => $e->getMessage().PHP_EOL.$e->getTraceAsString()]);
    }
}
