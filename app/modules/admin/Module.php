<?php

namespace app\modules\admin;

class Module extends \yii\base\Module
{
    public $layout = 'main';
    public $controllerNamespace = 'app\modules\admin\controllers';

    public $title = 'Bot Admin';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['modules/admin/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@app/modules/admin/messages',
            'fileMap' => [
                'modules/users/app' => 'app.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return \Yii::t('modules/admin/'.$category, $message, $params, $language);
    }
}
