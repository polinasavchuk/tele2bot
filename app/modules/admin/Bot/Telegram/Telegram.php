<?php

namespace app\modules\admin\Bot\Telegram;

use app\modules\admin\Bot\Bot;

/**
 * Class Telegram
 * @package app\modules\admin\Bot\Telegram
 *
 * @todo: Make single
 */
class Telegram extends \Longman\TelegramBot\Telegram
{
    /** @var \app\IQBot\Bot\Bot */
    public $bot;

    protected $command_namespace = '';

    public function __construct($api_key, $bot_name)
    {
        parent::__construct($api_key, $bot_name);

        $this->commands_config = [];
    }

    public function setCommandNamespace($namespace)
    {
        $this->command_namespace = $namespace;
    }

    public function getCommandObject($command)
    {
        $command_namespace = $this->command_namespace.$this->ucfirstUnicode($command) . 'Command';
        if (class_exists($command_namespace)) {
            return new $command_namespace($this, $this->update);
        }

        return null;
    }

    public function setBot(Bot $bot)
    {
        $this->bot = $bot;
    }
}
