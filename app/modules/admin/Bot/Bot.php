<?php

namespace app\modules\admin\Bot;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\TelegramLog;
use Longman\TelegramBot\Commands\Command;

use app\modules\admin\Bot\Telegram\Telegram;
use app\modules\admin\Bot\Telegram\Entities\EmptyServerResponse;
use app\modules\admin\models\State;

class Bot
{
    /** @var State */
    protected $state;

    protected $telegram;

    public function __construct(Telegram $telegram)
    {
        $this->telegram = $telegram;
        $this->telegram->setBot($this);
    }

    public function handle()
    {
        $this->telegram->handle();
    }

    public function handleGetUpdates()
    {
        $this->telegram->handleGetUpdates();
    }

    public function handleCommand(Command $command, $forward = false)
    {
        if (!$forward) {
            $this->loadState($command);
        }

        $controller = $this->resolveStateController();
        $controller->handle($command, $forward);

        return new EmptyServerResponse();
    }

    public function getState()
    {
        return $this->state;
    }

    public function setState($state, $command = null)
    {
        if (!$this->state) {
            if (!$command) {
                throw new \RuntimeException("Con't load state without command");
            }

            $this->loadState($command);
        }

        $this->state->updateAttributes(['state' => $state]);
    }

    protected function createState($id_chat, $id_user, $state = State::STATE_DEFAULT)
    {
        $state = new State();
        $state->id_chat = $id_chat;
        $state->id_user = $id_user;
        $state->state = State::STATE_DEFAULT;

        return $state;
    }

    protected function findState($id_chat, $id_user)
    {
        return State::findOne([
            'id_chat' => $id_chat,
            'id_user' => $id_user,
        ]);
    }

    protected function loadState(Command $command)
    {
        if (preg_match('/CallbackqueryCommand$/i', get_class($command))) {
            // В случае callback query чат может быть только приватным, и он не передается, находим сами

            $update = $command->getUpdate();
            $callbackQuery = $update->getCallbackQuery();
            $id_user = $callbackQuery->getFrom()->getId();
            $id_chat = $callbackQuery->getMessage()->getChat()->getId();
        } else {
            $message = $command->getMessage();
            $id_chat = $message->getChat()->getId();
            $id_user = $message->getFrom()->getId();
        }

        $state = $this->findState($id_chat, $id_user);

        if (!$state) {
            $state = $this->createState($id_chat, $id_user, State::STATE_DEFAULT);
            if (!$state->save()) {
                throw new \RuntimeException("Failed to save state\n".print_r($state->getErrors(), true));
            }
        }

        $this->state = $state;
    }

    /**
     * @todo: remove hardcoded namespace
     * @return mixed
     */
    protected function resolveStateController()
    {
        $state = $this->getState()->state;
        $className = 'app\\IQBot\\Controllers\\'.self::camelize($state).'StateController';
        if (!class_exists($className, true)) {
            throw new \RuntimeException("Failed to route state: '{$state}'");
        }

        return new $className($this);
    }

    public static function SendOrEditMessage($data)
    {
        return call_user_func(['\Longman\TelegramBot\Request', empty($data['message_id']) ? 'sendMessage' : 'editMessageText'], $data);
    }

    public static function SendMessage($data)
    {
        Request::sendMessage($data);
    }

    public static function SendPhoto($data, $filename = null, $content = null)
    {
        if ($filename && $content) {
            $fp = fopen(\Yii::$app->basePath . '/' . $filename, 'w+');
            fwrite($fp, $content);
            fflush($fp);
            rewind($fp);
            $data['photo'] = $fp;
        }

        $response = Request::sendPhoto($data);

        if ($response && $response->isOk()) {
            /** @var Message $message */
            $message = $response->getResult();
            /** @var Photo */
            $photo = $message->getPhoto();
            if ($photo) {
                return $photo[0]->getFileId();
            }
        }

        return false;
    }

    public static function SendDocument($data, $filename = null, $content = null)
    {
        if ($filename && $content) {
            $fp = fopen(\Yii::$app->basePath . '/' . $filename, 'w+');
            fwrite($fp, $content);
            fflush($fp);
            rewind($fp);
            $data['document'] = $fp;
        }

        $response = Request::sendDocument($data);

        if ($response && $response->isOk()) {
            /** @var Message $message */
            $message = $response->getResult();
            /** @var Document */
            $document = $message->getDocument();
            if ($document) {
                return $document->getFileId();
            }
        }

        return false;
    }

    protected static function camelize($word)
    {
        return str_replace(' ', '', ucwords(preg_replace('/[^A-Za-z0-9]+/', ' ', $word)));
    }
}
