<?php

namespace app\modules\admin\Bot\Controllers;

use app\IQBot\Bot\Bot;
use Longman\TelegramBot\Commands\Command;

abstract class StateController
{
    /** @var Bot */
    protected $bot;

    public function __construct(Bot $bot)
    {
        $this->bot = $bot;
    }

    public function forward($state, Command $command)
    {
        $this->bot->setState($state);
        return $this->bot->handleCommand($command, true);
    }

    abstract public function handle(Command $command, $forward = false);

    protected function getDefaultData(Command $command, &$from = null, &$message = null)
    {
        $message = null;
        $update = $command->getUpdate();
        switch ($updateType = $update->getUpdateType()) {
            case 'callback_query':
                $callbackQuery = $update->getCallbackQuery();
                $message = $callbackQuery->getMessage();
                $from = $callbackQuery->getFrom();
                break;
            case 'message':
                $message = $command->getMessage();
                $from = $message->getFrom();
                break;
        }

        if (!$message) {
            return null;
        }

        $data = [
            'chat_id' => $message->getChat()->getId(),
            'disable_web_page_preview' => true,
        ];

        if (isset($callbackQuery)) {
            $data['message_id'] = $callbackQuery->getMessage()->getMessageId();
        }

        if (!$message->getChat()->isPrivateChat()) {
            $data['reply_to_message_id'] = $message->getMessageId();
        }

        return $data;
    }
}
