<?php

namespace app\modules\admin\components;

use Exception;

class ValidationException extends Exception
{
    public $model;

    public function __construct($model = null, $message = "", $code = 0, Exception $previous = null)
    {
        $this->model = $model;
        parent::__construct($message, $code, $previous);
    }
}
