<?php


namespace app\modules\admin\components;

use app\modules\admin\Bot\Bot;

class BotComponent extends \yii\base\Component
{
    public $token;
    public $name;

    /** @var State */
    protected $state;

    public function init()
    {
        parent::init();

        if (!$this->token) {
            throw new \RuntimeException('Bot token not set');
        }

        if (!$this->name) {
            throw new \RuntimeException('Bot name not set');
        }

        if (\Yii::$app->db->driverName !== 'mysql') {
            throw new \RuntimeException('Only mysql drivers supported');
        }

        Bot::Initialize($this->token, $this->name, $this->getDatabaseConfig());
    }

    public static function sendMessage($data)
    {
        Bot::SendMessage($data);
    }

    public function sendPhoto($data, $filename = null, $content = null)
    {
        Bot::SendPhoto($data, $filename, $content);
    }

    public function sendDocument($data, $filename = null, $content = null)
    {
        Bot::SendDocument($data, $filename, $content);
    }

    protected function getDatabaseConfig()
    {
        if (!preg_match('/mysql:host=(.*);dbname=(.*)/iu', \Yii::$app->db->dsn, $m)) {
            throw new \RuntimeException('Invalid database dsn');
        }


        return [
            'host' => $m[1],
            'database' => $m[2],
            'user' => \Yii::$app->db->username,
            'password' => \Yii::$app->db->password,

        ];
    }
}
