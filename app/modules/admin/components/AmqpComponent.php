<?php

namespace app\modules\admin\components;

class AmqpComponent extends \yii\base\Component
{
    public $host;
    public $port = 5672;
    public $user;
    public $pass;
    public $path;

    public $queueName = 'messages';

    /** @var \PhpAmqpLib\Connection\AMQPStreamConnection */
    protected $_connection;

    /** @var \PhpAmqpLib\Channel\AMQPChannel */
    protected $_channel;

    public function __destruct()
    {
        if ($this->_channel) {
            $this->_channel->close();
            $this->_channel = null;
        }

        if ($this->_connection) {
            $this->_connection->close();
            $this->_connection = null;
        }
    }

    public function sendMessage($data)
    {
        $message = new \PhpAmqpLib\Message\AMQPMessage(serialize($data), ['delivery_mode' => 2]);
        $this->getChannel()->basic_publish($message, '', $this->queueName);
    }

    public function work($callback)
    {
        $channel = $this->getChannel();
        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($this->queueName, '', false, false, false, false, $callback);

        while(count($channel->callbacks)) {
            $channel->wait();
        }
    }

    protected function getConnection()
    {
        if (!$this->_connection) {
            $this->_connection = new \PhpAmqpLib\Connection\AMQPStreamConnection(
                $this->host,
                $this->port,
                $this->user,
                $this->pass,
                $this->path
            );
        }

        return $this->_connection;
    }

    protected function getChannel()
    {
        if (!$this->_channel) {
            $this->_channel = $this->getConnection()->channel();
            $this->_channel->queue_declare($this->queueName, false, true, false, false);
        }

        return $this->_channel;
    }
}
