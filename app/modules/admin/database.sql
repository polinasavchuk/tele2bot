DROP TABLE IF EXISTS admin_user;
CREATE TABLE admin_user (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  password_hash VARCHAR(255) NOT NULL,
  auth_key VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO admin_user (password_hash, auth_key) VALUES
  ('$2y$13$TxSkwT4Hj1/aodf0G7rkRerr6v1cC1GX89sG1aD3unkHQpDTgPn1O', '');

DROP TABLE IF EXISTS state;
CREATE TABLE state (
  id_chat BIGINT NOT NULL,
  id_user BIGINT NOT NULL,
  state VARCHAR(255) DEFAULT 'start' NOT NULL,
  created_at INT UNSIGNED NOT NULL,
  updated_at INT UNSIGNED DEFAULT NULL,
  PRIMARY KEY (id_chat, id_user)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
