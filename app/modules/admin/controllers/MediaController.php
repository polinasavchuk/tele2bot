<?php

namespace app\modules\admin\controllers;

use app\components\ViewImageAction;
use app\models\MediaMessage;
use app\models\MediaMessageSearch;
use yii\web\NotFoundHttpException;

class MediaController extends Controller
{
    public function actions()
    {
        return [
            'image' => [
                'class' => ViewImageAction::class,
            ],
        ];
    }

    public function actionSchedule()
    {
        $model = $this->getModelByType(MediaMessage::TYPE_SCHEDULE);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Данные сохранены');
            return $this->refresh();
        } else {
            return $this->render('schedule', [
                'model' => $model,
            ]);
        }
    }

    public function actionFlights()
    {
        $model = $this->getModelByType(MediaMessage::TYPE_FLIGHT);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Данные сохранены');
            return $this->refresh();
        } else {
            return $this->render('flights', [
                'model' => $model,
            ]);
        }
    }

    public function actionPhoto()
    {
        $model = $this->getModelByType(MediaMessage::TYPE_PHOTO);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Данные сохранены');
            return $this->refresh();
        } else {
            return $this->render('photo', [
                'model' => $model,
            ]);
        }
    }

    public function actionNotify()
    {
        $searchModel = new MediaMessageSearch();
        $dataProvider = $searchModel->search();

        return $this->render('notify', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddNotify()
    {
        $model = new MediaMessage();
        $model->type = MediaMessage::TYPE_NOTIFY;
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Данные сохранены');
            return $this->redirect(['notify']);
        } else {
            return $this->render('add-notify', [
                'model' => $model,
            ]);
        }
    }

    public function actionEditNotify($id)
    {
        $model = MediaMessage::find()->andWhere(['id' => $id, 'type' => MediaMessage::TYPE_NOTIFY])->one();
        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Данные сохранены');
            return $this->redirect(['notify']);
        } else {
            return $this->render('edit-notify', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeleteNotify($id)
    {
        $model = MediaMessage::find()->andWhere(['id' => $id, 'type' => MediaMessage::TYPE_NOTIFY])->one();
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $model->delete();

        \Yii::$app->session->setFlash('success', 'Уведомление удалено');
        return $this->redirect(['notify']);
    }

    protected function getModelByType($type)
    {
        $model = MediaMessage::find()->type($type)->one();
        if (!$model) {
            $model = new MediaMessage();
            $model->type = $type;
        }

        return $model;
    }
}