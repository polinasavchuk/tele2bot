<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\HookForm;
use app\modules\admin\Module;

class HookController extends Controller
{
    public function actionIndex()
    {
        $formModel = new HookForm();
        if ($formModel->load(\Yii::$app->request->post()) && $formModel->validate()) {
            $result = $formModel->setupHook();
            if ($result->isOk()) {
                \Yii::$app->session->setFlash('success', Module::t('app', 'Обработчик установлен'));
            } else {
                \Yii::$app->session->setFlash('error', Module::t('app', 'Ошибка при установке обработчика: {error}', ['error' => $result->getErrorCode()]));
            }

            return $this->refresh();
        }

        return $this->render('index', [
            'formModel' => $formModel,
        ]);
    }
}