<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\SpamForm;
use app\modules\admin\Module;

class SpamController extends Controller
{
    public function actionIndex()
    {
        $formModel = new SpamForm();
        if ($formModel->load(\Yii::$app->request->post()) && $formModel->validate()) {
            $formModel->send();

            \Yii::$app->session->setFlash('success', Module::t('app', 'Сообщение разослано'));
            $this->refresh();
        }

        return $this->render('index', [
            'formModel' => $formModel,
        ]);
    }
}
