<?php

namespace app\modules\admin\controllers;

use app\models\Speech;
use app\models\SpeechSearch;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class SpeechController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SpeechSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Speech();
        $model->loadDefaultValues();

        if ($this->createOrUpdate($model, \Yii::$app->request->post())) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->createOrUpdate($model, \Yii::$app->request->post())) {
            return $this->redirect(['index']);
        } else {
            $model->loadSpeakerIds();
            $model->trimSeconds();
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        \Yii::$app->db->transaction(function(Connection $db) use ($id) {
            $this->findModel($id)->delete();
            $db->createCommand()->delete('{{%speech_speaker}}', ['id_speech' => $id])->execute();
        });

        return $this->redirect(['index']);
    }

    protected function createOrUpdate(Speech $model, $data) : bool
    {
        if (!($model->load($data) && $model->validate())) {
            return false;
        }

        $trn = \Yii::$app->db->beginTransaction();

        try {
            if (!$model->save(false)) {
                throw new \RuntimeException('Failed to save model');
            }

            \Yii::$app->db->createCommand()->delete('{{%speech_speaker}}', 'id_speech = :id_speech', ['id_speech' => $model->id])->execute();

            if ($model->speakerIds) {
                $batchData = [];
                foreach ($model->speakerIds as $speakerId) {
                    $batchData[] = [$model->id, $speakerId];
                }

                \Yii::$app->db->createCommand()->batchInsert('{{%speech_speaker}}', ['id_speech', 'id_speaker'], $batchData)->execute();
            }

            $trn->commit();
        } catch (\Exception $e) {
            $trn->rollBack();
            throw $e;
        }

        return true;
    }

    protected function findModel($id)
    {
        if (($model = Speech::find()->where(['id' => $id])->with('speakers')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}