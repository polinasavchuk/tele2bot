<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\LoginForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'captcha'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'testLimit' => 1,
            ],
        ]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        $this->layout = 'base';

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $formModel = new LoginForm();

        if ($formModel->load(\Yii::$app->request->post()) && $formModel->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $formModel,
        ]);
    }

    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->goHome();
    }
}
