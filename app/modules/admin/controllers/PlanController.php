<?php

namespace app\modules\admin\controllers;

use app\components\ViewImageAction;
use app\models\Plan;
use yii\filters\VerbFilter;

class PlanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'image' => [
                'class' => ViewImageAction::class,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = $this->getModel();
        if (!$model) {
            $model = new Plan();
        }

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete()
    {
        Plan::deleteAll();
        return $this->redirect(['index']);
    }

    protected function getModel()
    {
        return Plan::find()->one();
    }
}