<?php

namespace app\modules\admin\commands;

use yii\console\Controller;
use yii\helpers\Console;

class InstallController extends Controller
{
    public function actionInstall()
    {
        $this->stdout("IQBot Telegram Admin installation script.\n");
        $this->stdout("Warning: Tables will be dropped, backup your data.\n\n", Console::FG_RED);

        if (!$this->confirm('Continue?')) {
            return;
        }

        $password = $this->prompt('Enter Admin password:', ['required' => true]);
        if (!$password) {
            $this->stdout("Canceled.\n");
        }

        $passwordHash = \Yii::$app->getSecurity()->generatePasswordHash($password);

        \Yii::$app->db->createCommand('DROP TABLE IF EXISTS admin_user')->execute();
        \Yii::$app->db->createCommand()->createTable(
            'admin_user',
            [
                'id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT',
                'password_hash' => 'VARCHAR(255) NOT NULL',
                'auth_key' => 'VARCHAR(255) NOT NULL',
                'PRIMARY KEY (id)',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci'
        )->execute();

        \Yii::$app->db->createCommand()->insert('admin_user', ['password_hash' => $passwordHash, 'auth_key' => ''])->execute();

        \Yii::$app->db->createCommand('DROP TABLE IF EXISTS state')->execute();
        \Yii::$app->db->createCommand()->createTable(
            'state',
            [
                'id_chat' => 'BIGINT NOT NULL',
                'id_user' => 'BIGINT NOT NULL',
                'state' => 'VARCHAR(255) DEFAULT \'start\' NOT NULL',
                'created_at' => 'INT UNSIGNED NOT NULL',
                'updated_at' => 'INT UNSIGNED NOT NULL',
                'PRIMARY KEY (id_chat, id_user)',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci'
        )->execute();

        $this->installI18n();

        $this->stdout("Done!\n");
    }

    protected function installI18n()
    {
        $this->stdout("Installing i18n tables!\n");

        \Yii::$app->db->createCommand('DROP TABLE IF EXISTS i18_source_message')->execute();
        \Yii::$app->db->createCommand()->createTable(
            'i18_source_message',
            [
                'id' => 'integer NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'category' => 'varchar(255)',
                'message' => 'text',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci'
        )->execute();

        \Yii::$app->db->createCommand('DROP TABLE IF EXISTS i18_message')->execute();
        \Yii::$app->db->createCommand()->createTable(
            'i18_message',
            [
                'id' => 'integer NOT NULL',
                'language' => 'varchar(16) NOT NULL',
                'translation' => 'text',
                'PRIMARY KEY (id, language)',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci'
        )->execute();

        \Yii::$app->db->createCommand('ALTER TABLE i18_message ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES i18_source_message (`id`) ON UPDATE CASCADE ON DELETE RESTRICT')->execute();
        \Yii::$app->db->createCommand('CREATE INDEX idx_message_language ON i18_message (language)')->execute();
        \Yii::$app->db->createCommand('CREATE INDEX idx_source_message_category ON i18_source_message (category)')->execute();
    }
}
