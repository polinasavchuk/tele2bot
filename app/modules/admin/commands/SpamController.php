<?php

namespace app\modules\admin\commands;

use app\IQBot\Bot\Config;
use app\modules\admin\Bot\Bot;
use app\modules\admin\Bot\Telegram\Telegram;
use app\modules\admin\models\Chat;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\TelegramLog;

class SpamController extends \yii\console\Controller
{
    public function actionListen()
    {
        TelegramLog::initUpdateLog('php://stderr');
        TelegramLog::initDebugLog('php://stderr');
        TelegramLog::initErrorLog('php://stderr');

        $telegram = new Telegram(Config::BOT_TOKEN, Config::BOT_NAME);
        $telegram->addCommandsPath(\Yii::getAlias('@app/IQBot/Bot/Command'));
        $telegram->setCommandNamespace('app\\IQBot\\Bot\\Command\\');
        $telegram->enableMySql(Config::GetDatabaseCredentials());

        \Yii::$app->amqp->work(function($message) {
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            $data = unserialize($message->body);
            if ($data) {
                try {
                    Request::sendMessage($data);
                } catch (\Exception $e) {
                    Request::sendMessage(['chat_id' => 92801842, 'text' => $e->getMessage().PHP_EOL.$e->getTraceAsString()]);
                }
            }
        });
    }

    public function actionSendText($chat_id, $message)
    {
        $message = preg_replace('/\\\\n/iu', "\n", $message);
        \Yii::$app->amqp->sendMessage(['chat_id' => $chat_id, 'text' => $message]);
    }
}