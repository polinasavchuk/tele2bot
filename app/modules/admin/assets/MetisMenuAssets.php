<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class MetisMenuAssets extends AssetBundle
{
    public $sourcePath = '@bower/metismenu/dist';

    public $css = [
        'metisMenu.min.css',
    ];

    public $js = [
        'metisMenu.min.js',
    ];
}
