<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class ModuleAssets extends AssetBundle
{
    public $depends = [
        'yii\web\YiiAsset',
        'app\modules\admin\assets\SbAdmin2Assets',
    ];
}
