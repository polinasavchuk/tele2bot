<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class SbAdmin2Assets extends AssetBundle
{
    public $sourcePath = '@bower/startbootstrap-sb-admin-2-blackrockdigital/dist';

    public $css = [
        'css/sb-admin-2.css',
    ];

    public $js = [
        'js/sb-admin-2.js',
    ];

    public $publishOptions = [
        'only' => [
            'js/*',
            'css/*',
        ]
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\modules\admin\assets\FontAwesomeAssets',
        'app\modules\admin\assets\MetisMenuAssets',
    ];
}
