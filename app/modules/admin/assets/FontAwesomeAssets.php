<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class FontAwesomeAssets extends AssetBundle
{
    public $sourcePath = '@bower/font-awesome';

    public $css = [
        'css/font-awesome.min.css',
    ];

    public $publishOptions = [
        'only' => [
            'fonts/*',
            'css/*',
        ]
    ];
}