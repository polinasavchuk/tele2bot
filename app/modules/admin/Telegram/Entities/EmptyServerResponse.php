<?php

namespace app\modules\admin\Telegram\Entities;

use Longman\TelegramBot\Entities\ServerResponse;

class EmptyServerResponse extends ServerResponse
{
    public function __construct()
    {
    }

    public function isOk()
    {
        return true;
    }
}
