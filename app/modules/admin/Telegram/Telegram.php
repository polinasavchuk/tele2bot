<?php

namespace app\modules\admin\Telegram\Telegram;

use app\modules\admin\Bot\Bot;

class Telegram extends \Longman\TelegramBot\Telegram
{
    /** @var Bot */
    public $bot;

    public function __construct($api_key, $bot_name)
    {
        parent::__construct($api_key, $bot_name);
        parent::addCommandsPath(dirname(__FILE__).'/../Bot/Command');
    }

    /**
     * Запрещаем добавлять команды из библиотеки
     */
    public function addCommandsPath($path, $before = true)
    {
        return $this;
    }

    public function getCommandObject($command)
    {
        $command_namespace = 'app\\Bot\\Command\\' . $this->ucfirstUnicode($command) . 'Command';
        if (class_exists($command_namespace)) {
            return new $command_namespace($this, $this->update);
        }

        return null;
    }

    public function setBot(Bot $bot)
    {
        $this->bot = $bot;
    }
}