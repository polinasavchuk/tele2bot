<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $searchModel app\modules\admin\models\MessageSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Module::t('app', 'Сообщения');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="message-index">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
//                'chat_id',
//                'id',
//                'user_id',
                'date',
                [
                    'label' => Module::t('app', 'Пользователь'),
                    'format' => 'raw',
                    'value' => function (\app\modules\admin\models\Message $model) {
                        $user = $model->user;
                        $text = '';
                        if ($user->first_name) {
                            $text .= Html::encode($user->first_name);
                        }

                        if ($user->last_name) {
                            $text .= ' '.Html::encode($user->last_name);
                        }

                        if ($user->username) {
                            $text .= $text ? '<br>' : '';
                            $text .= '@'.Html::encode($user->username);
//                            $text .= Html::a('@'.$username, 'https://telegram.me/'.$username, ['target' => '_blank']);
                        }

                        return $text;
                    }
                ],
//                [
//                    'label' => Module::t('app', 'Тип чата'),
//                    'attribute' => 'chat.typeName',
//                ],
//                'forward_from',
                // 'forward_from_chat',
                // 'forward_date',
                // 'reply_to_chat',
                // 'reply_to_message',
                 'text:ntext',
                // 'entities:ntext',
                // 'audio:ntext',
                // 'document:ntext',
                // 'photo:ntext',
                // 'sticker:ntext',
                // 'video:ntext',
                // 'voice:ntext',
                // 'contact:ntext',
                // 'location:ntext',
                // 'venue:ntext',
                // 'caption:ntext',
                // 'new_chat_member',
                // 'left_chat_member',
                // 'new_chat_title',
                // 'new_chat_photo:ntext',
                // 'delete_chat_photo',
                // 'group_chat_created',
                // 'supergroup_chat_created',
                // 'channel_chat_created',
                // 'migrate_to_chat_id',
                // 'migrate_from_chat_id',
                // 'pinned_message:ntext',

//                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>

</div>
