<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model app\modules\admin\models\Message
 */

$this->title = $model->chat_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-view">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'chat_id' => $model->chat_id, 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'chat_id' => $model->chat_id, 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'chat_id',
                'id',
                'user_id',
                'date',
                'forward_from',
                'forward_from_chat',
                'forward_date',
                'reply_to_chat',
                'reply_to_message',
                'text:ntext',
                'entities:ntext',
                'audio:ntext',
                'document:ntext',
                'photo:ntext',
                'sticker:ntext',
                'video:ntext',
                'voice:ntext',
                'contact:ntext',
                'location:ntext',
                'venue:ntext',
                'caption:ntext',
                'new_chat_member',
                'left_chat_member',
                'new_chat_title',
                'new_chat_photo:ntext',
                'delete_chat_photo',
                'group_chat_created',
                'supergroup_chat_created',
                'channel_chat_created',
                'migrate_to_chat_id',
                'migrate_from_chat_id',
                'pinned_message:ntext',
            ],
        ]) ?>
        </div>
    </div>
</div>
