<?php

/**
 * @var $this yii\web\View
 * @var $searchModel app\modules\admin\models\UserSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */


use yii\helpers\Html;
use yii\grid\GridView;

use \app\modules\admin\Module;

$this->title = Module::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'first_name',
                'last_name',
                [
                    'format' => 'raw',
                    'attribute' => 'username',
                    'value' => function (\app\modules\admin\models\User $model) {
                        if ($model->username) {
                            return Html::a(Html::encode($model->username),
                                'https://telegram.me/'.Html::encode($model->username),
                                ['target' => '_blank']);
                        }

                        return null;
                    }
                ],
                'profile.phone',
                [
                    'attribute' => 'created_at',
                    'filter' => false,
                ],
                [
                    'attribute' => 'updated_at',
                    'filter' => false,
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{messages}',
                    'buttons' => [
                        'messages' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-comments fa-fw"></i>',
                                ['message/index', 'MessageSearch[user_id]' => $model->id],
                                ['target' => '_blank', 'title' => Module::t('app', 'Сообщения пользователя')]);
                        },
                        'send' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-share-square-o" aria-hidden="true"></i>',
                                ['spam/send', 'id' => $model->id],
                                ['target' => '_blank', 'title' => Module::t('app', 'Отправить сообщение')]);
                        }
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
