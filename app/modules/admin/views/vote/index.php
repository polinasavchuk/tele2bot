<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $searchModel \app\models\Vote
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Module::t('app', 'Голосование');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="speaker-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'user.first_name',
                    'user.last_name',
                    [
                        'format' => 'raw',
                        'attribute' => 'user.username',
                        'value' => function (\app\models\Vote $model) {
                            $model = $model->user;
                            if ($model->username) {
                                return Html::a(Html::encode($model->username),
                                    'https://telegram.me/'.Html::encode($model->username),
                                    ['target' => '_blank']);
                            }

                            return null;
                        }
                    ],
                    [
                        'attribute' => 'result',
                        'format' => 'boolean',
                        'filter' => false,
                    ],
                    'note:ntext',
                ],
            ]); ?>
        </div>
    </div>

</div>
