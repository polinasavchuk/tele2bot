<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datetime\DateTimePicker;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $model app\models\Speech
 * @var $form yii\widgets\ActiveForm
 */
?>

<div class="rock-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'id_hall')->dropDownList(
        app\models\Hall::find()->select(['name'])->indexBy('id')->column(), [
        'prompt' => Module::t('app', ''),
    ]); ?>
    <?= $form->field($model, 'speakerIds')->widget(\kartik\select2\Select2::class, [
        'data' => \app\models\Speaker::find()->select(['name'])->indexBy('id')->column(),
        'language' => 'ru',
        'options' => [
            'multiple' => true,
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'start')->widget(DateTimePicker::class, [
                'pluginOptions' => [
                    'language' => 'ru',
                    'autoclose' => true,
                    'weekStart' => 1,
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'end')->widget(DateTimePicker::class, [
                'pluginOptions' => [
                    'language' => 'ru',
                    'autoclose' => true,
                    'weekStart' => 1,
                ],
            ]); ?>
        </div>
    </div>

    <?= $form->field($model, 'about')->textarea(['rows' => 6]); ?>
    <?= $form->field($model, 'active')->checkbox(); ?>

    <div class="form-group" style="margin-top: 20px;">
        <?= Html::submitButton($model->isNewRecord ? Module::t('app', 'Создать') : Module::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
