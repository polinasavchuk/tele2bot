<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $searchModel \app\models\Speech
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Module::t('app', 'Доклады');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="speaker-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p><?= Html::a(Module::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success']) ?></p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'name',
                    [
                        'header' => 'Спикер',
                        'value' => function (\app\models\Speech $model) {
                            if ($model->speakers) {
                                return implode(', ', array_column($model->speakers, 'name'));
                            }

                            return null;
                        }
                    ],
                    [
                        'format' => 'raw',
                        'header' => 'Время',
                        'value' => function (\app\models\Speech $model) {
                            $model->trimSeconds();
                            return "{$model->start} &mdash; {$model->end}";
                        },
                    ],
                    [
                        'attribute' => 'hall.name',
                        'header' => 'Зал',
                    ],
                    [
                        'attribute' => 'active',
                        'format' => 'boolean',
                        'filter' => false,
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
