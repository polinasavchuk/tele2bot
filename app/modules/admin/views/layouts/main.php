<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use \yii\helpers\Url;
use yii\bootstrap\Alert;

use \app\modules\admin\Module;
?>
<?php $this->beginContent('@app/modules/admin/views/layouts/base.php'); ?>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=Url::to('default/index');?>"><?=Module::getInstance()->getTitle();?></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="<?=Url::to(['admin/logout']);?>"><i class="fa fa-sign-out fa-fw"></i> Выйти</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="<?=Url::to(['user/index']);?>"><i class="fa fa-user fa-fw"></i> Пользователи</a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['message/index']);?>"><i class="fa fa-comments fa-fw"></i> Сообщения</a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['spam/index']);?>"><i class="fa fa-envelope fa-fw"></i> Рассылка</a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['media/notify']);?>"><i class="fa fa-envelope fa-fw"></i> Уведомления</a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['media/schedule']);?>"><i class="fa fa-comments fa-fw"></i> Расписание</a>
                    </li>
<?php /*
                    <li>
                        <a href="<?=Url::to(['speaker/index']);?>"><i class="fa fa-users fa-fw"></i> Спикеры</a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['speech/index']);?>"><i class="fa fa-comments fa-fw"></i> Доклады</a>
                    </li>
*/ ?>
                    <li>
                        <a href="<?=Url::to(['media/flights']);?>"><i class="fa fa-plane fa-fw"></i> Рейсы</a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['media/photo']);?>"><i class="fa fa-camera fa-fw"></i> Фото</a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['vote/index']);?>"><i class="fa fa-bar-chart fa-fw"></i> Голосование</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" style="margin-top: 20px;">
                    <?=\yii\widgets\Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'options' => [
                            'class' => 'breadcrumb',
                            'style' => 'margin-bottom: 0'
                        ],
                    ])?>
                </div>
            </div>


        <?php if ($flashes = Yii::$app->session->getAllFlashes()): ?>
            <div class="row">
                <div class="col-lg-12" style="margin-top: 20px;">
                    <?php foreach ($flashes as $key => $message): ?>
                        <?php if (is_array($message)): ?>
                            <?php foreach ($message as $m): ?>
                                <?=Alert::widget([
                                    'options' => [
                                        'class' => 'alert-'.$key,
                                    ],
                                    'body' => $m,
                                ]);?>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?=Alert::widget([
                                'options' => [
                                    'class' => 'alert-'.$key,
                                ],
                                'body' => $message,
                            ]);?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif;?>

            <?=$content; ?>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php $this->endContent(); ?>
