<?php

/**
 * @var $this yii\web\View
 * @var \app\modules\admin\models\HookForm $formModel
 */


use yii\helpers\Html;
use \yii\bootstrap\ActiveForm;

use \app\modules\admin\Module;

$this->title = Module::t('app', 'Установка обработчика Telegram');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($formModel, 'token'); ?>
        <?= $form->field($formModel, 'hookUrl'); ?>
        <?= $form->field($formModel, 'name'); ?>

        <div class="form-group">
            <?= Html::submitButton(Module::t('app', 'Установить'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
