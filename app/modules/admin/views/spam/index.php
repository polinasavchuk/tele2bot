<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

use app\modules\admin\Module;

/**
 * @var yii\web\View $this
 * @var \app\modules\admin\models\SpamForm $formModel
 */

$this->title = Module::t('app', 'Рассылки');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?=Alert::widget([
            'body' => Module::t('app', 'Сообщение будет разослано во все чаты'),
            'closeButton' => false,
            'options' => [
                'class' => 'alert-warning',
            ]
        ]);?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
    <?php
        /** @var ActiveForm $form */
        $form = ActiveForm::begin();
    ?>

        <?=$form->field($formModel, 'message')->textarea(['rows' => 6]);?>

        <div class="form-group">
            <?= Html::submitButton(Module::t('app', 'Отправить'), ['class' => 'btn btn-primary', 'data-confirm' => Module::t('app', 'Отправить сообщение?')]) ?>
        </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
