<?php

use yii\helpers\Html;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $model app\models\Exhibitor
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Экспоненты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rock-update">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
