<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $model app\models\Speaker
 * @var $form yii\widgets\ActiveForm
 */
?>

<div class="rock-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'about')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'active')->checkbox() ?>

    <fieldset>
        <label>Фото</label>

        <div class="row">
                <?php if ($model->id_file): ?>
                    <div class="col-lg-2">
                        <?=Html::a(Html::img(Url::to(['image', 'id' => $model->id_file]), ['style' => 'width: 100%']),
                            ['image', 'id' => $model->id_file], ['target' => '_blank', 'title' => 'Фото']);?>
                    </div>
                <?php endif; ?>

                <div class="col-lg-10">
                    <?= $form->field($model, 'imageFile')->fileInput()->label('Загрузить') ?>
                    <?php if ($model->id_file): ?>
                        <?= $form->field($model, 'deleteImage')->checkbox();?>
                    <?php endif; ?>
                </div>
        </div>
    </fieldset>

    <div class="form-group" style="margin-top: 20px;">
        <?= Html::submitButton($model->isNewRecord ? Module::t('app', 'Создать') : Module::t('app', 'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
