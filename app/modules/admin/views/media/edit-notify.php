<?php

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $model app\models\MediaMessage
 */

$this->title = Module::t('app', 'Редактировать уведомление');
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Уведомления'), 'url' => ['notify']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="speaker-create">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Module::t('app', 'Редактировать уведомление'); ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
