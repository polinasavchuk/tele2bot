<?php

/**
 * @var $this yii\web\View
 * @var $model app\models\MediaMessage
 */

use app\modules\admin\Module;
use yii\helpers\Html;

?>

<div class="plan-form">

    <?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-12"><?=Html::errorSummary($model);?></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'pickerTime')->widget(\kartik\datetime\DateTimePicker::class, [
                'pluginOptions' => [
                    'language' => 'ru',
                    'autoclose' => true,
                    'weekStart' => 1,
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'message')->textarea(['rows' => 6]);?>
        </div>
    </div>

    <hr>
    <div class="row">
        <?php if ($model->id_file): ?>
            <div class="col-lg-4">
                <?=Html::a(Html::img(\yii\helpers\Url::to(['image', 'id' => $model->id_file]), ['style' => 'width: 100%']),
                    ['image', 'id' => $model->id_file], ['target' => '_blank', 'title' => 'Фото']);?>
            </div>
        <?php endif; ?>

        <div class="col-lg-8">
            <?= $form->field($model, 'imageFile')->fileInput()->label('Загрузить') ?>
        </div>
    </div>

    <div class="form-group" style="margin-top: 20px;">
        <?= Html::submitButton(Module::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php \yii\bootstrap\ActiveForm::end(); ?>

</div>
