<?php

use yii\helpers\Html;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $model app\models\MediaMessage
 */

$this->title = Module::t('app', 'Расписание рейсов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="speaker-create">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Module::t('app', 'Расписание рейсов'); ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="plan-form">

                <?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-lg-12"><?=Html::errorSummary($model);?></div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <?= $form->field($model, 'message')->textarea();?>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 20px;">
                        <?php if ($model->id_file): ?>
                            <div class="col-lg-4">
                                <?=Html::a(Html::img(\yii\helpers\Url::to(['image', 'id' => $model->id_file]), ['style' => 'width: 100%']),
                                    ['image', 'id' => $model->id_file], ['target' => '_blank', 'title' => 'Фото']);?>
                            </div>
                        <?php endif; ?>

                        <div class="col-lg-8">
                            <?= $form->field($model, 'imageFile')->fileInput()->label('Загрузить') ?>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 20px;">
                        <?= Html::submitButton(Module::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>

                    <?php /* if (!$model->isNewRecord): ?>
                        <?= Html::a(Module::t('app', 'Удалить'), ['delete'], [
                            'style' => 'margin-left: 20px',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-form' => '0',
                        ]); ?>
                    <?php endif; */ ?>

                    </div>

                <?php \yii\bootstrap\ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>
