<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\modules\admin\Module;

/**
 * @var $this yii\web\View
 * @var $searchModel \app\models\Exhibitor
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Module::t('app', 'Уведомления');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="speaker-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p><?= Html::a(Module::t('app', 'Добавить'), ['add-notify'], ['class' => 'btn btn-success']) ?></p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'pickerTime',
                    ],
                    'message:ntext',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['edit-notify', 'id' => $model->id], ['title' => 'Редактировать', 'data-pjax' => 0]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-notify', 'id' => $model->id], ['title' => 'Редактировать', 'data-pjax' => 0]);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
