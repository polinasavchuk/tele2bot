<?php
/**
 * @var \app\modules\admin\models\LoginForm $model
 * @var \yii\web\View $this
 */

use yii\bootstrap\ActiveForm;
?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Авторизация</h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <fieldset>

                        <?=$form->field($model, 'password')->passwordInput(
                            ['placeholder' => $model->getAttributeLabel('password')]); ?>

                        <?=$form->field($model, 'captcha')->widget(\yii\captcha\Captcha::className(), [
                            'captchaAction' => 'admin/captcha',
                        ]);?>
                        <!-- Change this to a button or input when using this as a form -->

                        <?=\yii\helpers\Html::submitButton('Войти', ['class' => 'btn btn-lg btn-success btn-block']);?>
                    </fieldset>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
