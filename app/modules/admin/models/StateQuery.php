<?php

namespace app\modules\admin\models;

/**
 * This is the ActiveQuery class for [[State]].
 *
 * @see State
 */
class StateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return State[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return State|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function chat($id_chat)
    {
        return $this->andWhere(['id_chat' => $id_chat]);
    }

    public function user($id_user)
    {
        return $this->andWhere(['id_user' => $id_user]);
    }

    public function privateChat()
    {
        return $this->join('INNER JOIN', ['_chat' => 'chat'], '_chat.id = state.id_chat AND _chat.type = "private"');
    }
}
