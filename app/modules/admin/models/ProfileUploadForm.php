<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\web\UploadedFile;

class ProfileUploadForm extends Model
{
    /** @var UploadedFile */
    public $file;

    public $full = false;

    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file'],
            [['full'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
            'full' => 'Полная загрузка',
        ];
    }

    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->file = UploadedFile::getInstance($this, 'file');
            return true;
        }

        return false;
    }

    public function import()
    {
        try {
            $fp = fopen($this->file->tempName, 'r');
            if (!$fp) {
                $this->addError('file', 'Не удалось открыть файл');
            }

            $data = [];

            $error = null;
            $emailValidator = new \yii\validators\EmailValidator();

            while ($row = fgetcsv($fp)) {
                $number = intval($row[0], 10);
                $email = trim($row[6]);
                $phone = self::preparePhone($row[7]);

                if (!$number) {
                    continue;
                }

                if ($email && !$emailValidator->validate($email, $error)) {
                    $this->throwValidationError($error, $row);
                }

                $data[] = [
                    'number' => $number,
                    'lastname' => trim($row[1]),
                    'firstname' => trim($row[2]),
                    'patronymic' => trim($row[3]),
                    'company' => trim($row[4]),
                    'position' => trim($row[5]),
                    'email' => $email,
                    'phone' => $phone,
                ];
            }

            fclose($fp);


            $stmt = \Yii::$app->db->createCommand(
                'INSERT INTO {{%profile}} (number, lastname, firstname, patronymic, company, position, email, phone) '.
                'VALUES (:number, :lastname, :firstname, :patronymic, :company, :position, :email, :phone) ON DUPLICATE KEY UPDATE '.
                'lastname = VALUES(lastname), firstname = VALUES(firstname), patronymic = VALUES(patronymic), '.
                'company = VALUES(company), position = VALUES(position), email = VALUES(email), phone = VALUES(phone)'
            );

            $stmt->prepare();

            $trn = \Yii::$app->db->beginTransaction();

            try {
                if ($this->full) {
                    \Yii::$app->db->createCommand()->delete('{{%profile}}')->execute();
                    \Yii::$app->db->createCommand()->delete('{{%user_profile}}')->execute();
                }

                foreach ($data as $d) {
                    $stmt->bindValues($d);
                    $stmt->execute();
                }

                $trn->commit();
            } catch (\Exception $e) {
                $trn->rollBack();
                throw $e;
            }

            return true;
        } catch (\Exception $e) {
            $this->addError('file', $e->getMessage());
            return false;
        }
    }

    public static function preparePhone($phone)
    {
        $phone = preg_replace('/[^0-9]/iu', '', $phone);
        $phone = preg_replace('/^(8|7)/iu', '+7', $phone);
        $phone = preg_replace('/^9/iu', '+79', $phone);

        if ($phone && preg_match('/^\d/iu', $phone)) {
            $phone = '+'.$phone;
        }

        return $phone;
    }

    protected function throwValidationError($error, $row)
    {
        throw new \RuntimeException("Ошибка: {$error}; При импорте строки ".implode(',', $row));
    }
}
