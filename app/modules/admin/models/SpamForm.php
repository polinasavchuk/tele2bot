<?php

namespace app\modules\admin\models;

use app\models\Profile;
use app\modules\admin\Bot\Bot;
use app\modules\admin\Module;
use yii\base\Model;
use yii\db\Query;

class SpamForm extends Model
{
    public $id_chat;
    public $message;

    public function rules()
    {
        return [
            [['message'], 'required'],
        ];
    }

    public function send(array $data = [])
    {
        $query = (new Query())
            ->from(Chat::tableName())
            ->select(['id', 'type'])
            ->orderBy('id DESC');

        foreach ($query->each() as $chat) {
            $message = $this->message;
            \Yii::$app->amqp->sendMessage(array_merge([
                'chat_id' => $chat['id'],
                'text' => $message,
            ], $data));
        }
    }

    public function attributeLabels()
    {
        return [
            'id_chat' => Module::t('app', 'Чат'),
            'message' => Module::t('app', 'Сообщение'),
        ];
    }
}