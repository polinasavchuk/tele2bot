<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "telegram_update".
 *
 * @property string $id
 * @property integer $chat_id
 * @property string $message_id
 * @property string $inline_query_id
 * @property string $chosen_inline_result_id
 * @property string $callback_query_id
 * @property string $edited_message_id
 *
 * @property Message $chat
 * @property InlineQuery $inlineQuery
 * @property ChosenInlineResult $chosenInlineResult
 * @property CallbackQuery $callbackQuery
 * @property EditedMessage $editedMessage
 */
class TelegramUpdate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_update';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'chat_id', 'message_id', 'inline_query_id', 'chosen_inline_result_id', 'callback_query_id', 'edited_message_id'], 'integer'],
            [['chat_id', 'message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::className(), 'targetAttribute' => ['chat_id' => 'chat_id', 'message_id' => 'id']],
            [['inline_query_id'], 'exist', 'skipOnError' => true, 'targetClass' => InlineQuery::className(), 'targetAttribute' => ['inline_query_id' => 'id']],
            [['chosen_inline_result_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChosenInlineResult::className(), 'targetAttribute' => ['chosen_inline_result_id' => 'id']],
            [['callback_query_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallbackQuery::className(), 'targetAttribute' => ['callback_query_id' => 'id']],
            [['edited_message_id'], 'exist', 'skipOnError' => true, 'targetClass' => EditedMessage::className(), 'targetAttribute' => ['edited_message_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'message_id' => Yii::t('app', 'Message ID'),
            'inline_query_id' => Yii::t('app', 'Inline Query ID'),
            'chosen_inline_result_id' => Yii::t('app', 'Chosen Inline Result ID'),
            'callback_query_id' => Yii::t('app', 'Callback Query ID'),
            'edited_message_id' => Yii::t('app', 'Edited Message ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Message::className(), ['chat_id' => 'chat_id', 'id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInlineQuery()
    {
        return $this->hasOne(InlineQuery::className(), ['id' => 'inline_query_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChosenInlineResult()
    {
        return $this->hasOne(ChosenInlineResult::className(), ['id' => 'chosen_inline_result_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallbackQuery()
    {
        return $this->hasOne(CallbackQuery::className(), ['id' => 'callback_query_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditedMessage()
    {
        return $this->hasOne(EditedMessage::className(), ['id' => 'edited_message_id']);
    }
}
