<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "callback_query".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $chat_id
 * @property string $message_id
 * @property string $inline_message_id
 * @property string $data
 * @property string $created_at
 *
 * @property User $user
 * @property Message $chat
 * @property TelegramUpdate[] $telegramUpdates
 */
class CallbackQuery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'callback_query';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'user_id', 'chat_id', 'message_id'], 'integer'],
            [['created_at'], 'safe'],
            [['inline_message_id', 'data'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['chat_id', 'message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::className(), 'targetAttribute' => ['chat_id' => 'chat_id', 'message_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'message_id' => Yii::t('app', 'Message ID'),
            'inline_message_id' => Yii::t('app', 'Inline Message ID'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Message::className(), ['chat_id' => 'chat_id', 'id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramUpdates()
    {
        return $this->hasMany(TelegramUpdate::className(), ['callback_query_id' => 'id']);
    }
}
