<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "edited_message".
 *
 * @property string $id
 * @property integer $chat_id
 * @property string $message_id
 * @property integer $user_id
 * @property string $edit_date
 * @property string $text
 * @property string $entities
 * @property string $caption
 *
 * @property Chat $chat
 * @property Message $chat0
 * @property User $user
 * @property TelegramUpdate[] $telegramUpdates
 */
class EditedMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edited_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'message_id', 'user_id'], 'integer'],
            [['edit_date'], 'safe'],
            [['text', 'entities', 'caption'], 'string'],
            [['chat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chat::className(), 'targetAttribute' => ['chat_id' => 'id']],
            [['chat_id', 'message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::className(), 'targetAttribute' => ['chat_id' => 'chat_id', 'message_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'message_id' => Yii::t('app', 'Message ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'edit_date' => Yii::t('app', 'Edit Date'),
            'text' => Yii::t('app', 'Text'),
            'entities' => Yii::t('app', 'Entities'),
            'caption' => Yii::t('app', 'Caption'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat0()
    {
        return $this->hasOne(Message::className(), ['chat_id' => 'chat_id', 'id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramUpdates()
    {
        return $this->hasMany(TelegramUpdate::className(), ['edited_message_id' => 'id']);
    }
}
