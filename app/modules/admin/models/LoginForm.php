<?php

namespace app\modules\admin\models;

use yii\base\Model;

class LoginForm extends Model
{
    public $password;
    public $captcha;

    /** @var AdminUser */
    protected $_user;

    public function rules()
    {
        return [
            [['password'], 'required'],
            [['password'], 'validatePassword'],
            [['captcha'], 'captcha', 'captchaAction' => 'admin/admin/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'captcha' => 'Проверочный код',
        ];
    }


    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return \Yii::$app->user->login($this->getUser());
        } else {
            return false;
        }
    }

    protected function getUser()
    {
        if (!$this->_user) {
            $this->_user = AdminUser::findByPassword($this->password);
        }

        return $this->_user;
    }
}
