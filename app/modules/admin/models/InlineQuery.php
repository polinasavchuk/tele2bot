<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "inline_query".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $location
 * @property string $query
 * @property string $offset
 * @property string $created_at
 *
 * @property User $user
 * @property TelegramUpdate[] $telegramUpdates
 */
class InlineQuery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inline_query';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'query'], 'required'],
            [['id', 'user_id'], 'integer'],
            [['query'], 'string'],
            [['created_at'], 'safe'],
            [['location', 'offset'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'location' => Yii::t('app', 'Location'),
            'query' => Yii::t('app', 'Query'),
            'offset' => Yii::t('app', 'Offset'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramUpdates()
    {
        return $this->hasMany(TelegramUpdate::className(), ['inline_query_id' => 'id']);
    }
}
