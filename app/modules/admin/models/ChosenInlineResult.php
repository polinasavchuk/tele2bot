<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "chosen_inline_result".
 *
 * @property string $id
 * @property string $result_id
 * @property integer $user_id
 * @property string $location
 * @property string $inline_message_id
 * @property string $query
 * @property string $created_at
 *
 * @property User $user
 * @property TelegramUpdate[] $telegramUpdates
 */
class ChosenInlineResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chosen_inline_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['query'], 'required'],
            [['query'], 'string'],
            [['created_at'], 'safe'],
            [['result_id', 'location', 'inline_message_id'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'result_id' => Yii::t('app', 'Result ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'location' => Yii::t('app', 'Location'),
            'inline_message_id' => Yii::t('app', 'Inline Message ID'),
            'query' => Yii::t('app', 'Query'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTelegramUpdates()
    {
        return $this->hasMany(TelegramUpdate::className(), ['chosen_inline_result_id' => 'id']);
    }
}
