<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "state".
 *
 * @property integer $id_chat
 * @property integer $id_user
 * @property string $state
 * @property integer $created_at
 * @property integer $updated_at
 */
class State extends \yii\db\ActiveRecord
{
    const STATE_DEFAULT = 'start';
    const STATE_VOTE    = 'vote';
    const STATE_VOTE_2  = 'vote_2';
    const STATE_VOTE_3  = 'vote_3';
    const STATE_VOTE_4  = 'vote_4';

    const STATE_AUTH = 'auth';
    const STATE_AUTH_EMAIL = 'auth_email';
    const STATE_AUTH_PHONE = 'auth_phone';
    const STATE_AUTH_TICKET = 'auth_ticket';
    const STATE_ASK_SPEAKER = 'ask_speaker';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_chat', 'id_user'], 'required'],
            [['id_chat', 'id_user', 'created_at', 'updated_at'], 'integer'],
            [['state'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_chat' => 'Id Chat',
            'id_user' => 'Id User',
            'state' => 'State',
            'ts' => 'Ts',
        ];
    }

    /**
     * @inheritdoc
     * @return StateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StateQuery(get_called_class());
    }

    public static function Set($id_user, $id_chat, $state)
    {
        /** @var State $model */
        $model = self::find()->user($id_user)->chat($id_chat)->one();
        if (!$model) {
            $model = new self();
            $model->id_user = $id_user;
            $model->id_chat = $id_chat;
        }

        $model->state = $state;
        $model->save(false);

        return $model;
    }

    public static function Get($id_user, $id_chat, $defaultState = 'start')
    {
        $model = self::find()->user($id_user)->chat($id_chat)->one();
        if (!$model) {
            $model = self::Set($id_user, $id_chat, $defaultState);
        }

        return $model;
    }
}
