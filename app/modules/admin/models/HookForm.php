<?php

namespace app\modules\admin\models;

use app\IQBot\Bot\Config;
use app\modules\admin\Module;
use yii\base\Model;
use yii\helpers\Url;

class HookForm extends Model
{
    public $token = Config::BOT_TOKEN;
    public $name = Config::BOT_NAME;

    protected $_hookUrl;

    public function getHookUrl()
    {
        if ($this->_hookUrl === null) {
            $this->_hookUrl = Url::to(['/hook/index'], 'https');
        }

        return $this->_hookUrl;
    }

    public function setHookUrl($value)
    {
        $this->_hookUrl = $value;
    }

    public function attributeLabels()
    {
        return [
            'token' => Module::t('app', 'Токен'),
            'name' => Module::t('app', 'Имя бота'),
            'hookUrl' => Module::t('app', 'URL обработчика'),
        ];
    }

    public function rules()
    {
        return [
            [['token', 'name', 'hookUrl'], 'required'],
            [['hookUrl'], 'url', 'validSchemes' => ['https']],
        ];
    }

    public function setupHook()
    {
        $telegram = new \Longman\TelegramBot\Telegram($this->token, $this->name);
        $result = $telegram->setWebHook($this->hookUrl);
        return $result;
    }
}
