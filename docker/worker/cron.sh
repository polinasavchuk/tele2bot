#!/bin/sh

printenv | sed 's/^\([a-zA-Z0-9_]*\)=\(.*\)$/export \1="\2"/g' > /root/setenv.sh && chmod +x /root/setenv.sh

/usr/sbin/cron

trap "service cron stop; exit" SIGINT SIGTERM

touch /var/log/cron.log
tail -f /var/log/cron.log && wait $!
