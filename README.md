# Шаблон telegram бота и административного интерфейса

## Установка

1. Прописать в `app/IQBot/Bot/Config.php` токен и имя бота
2. Запустить docker:
```
sudo docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
```
3. Создать базу данных:
```
sudo docker-compose exec db bash
mysql -uroot -p
CREATE DATABASE iqbot;
USE iqbot;
```
4. Создать служебные таблицы из файла `app/vendor/longman/telegram-bot/structure.sql`
5. Создать таблицы для работы административного интерфейса:
```
sudo docker-compose exec web bash
cd ..
./yii iqbot-install/install
```

